# PuzzleFlex.jl

Analyze extreme configurations of large systems of loosely connected rigid bodies using linear programming.

## Installation

`] add https://gitlab.com/dartmouthrobotics/PuzzleFlex.jl`

Or to install from source

1. clone the repository
2. launch the repl in the project context `julia --project`
3. install the dependencies `] instantiate`
4. exit the repl and run the command to install the conda dependencies: `bash scripts/ setup_qt5_conda.sh`

## DOCUMENTATION

Please see the documentation for details on usage.

[Here](https://dartmouthrobotics.gitlab.io/PuzzleFlex.jl)

## Example

```julia
using PuzzleFlex
import GeometryTypes

# sets the maximum error used for selecting time steps
# between solutions of the linear program
maximum_error = 0.0

chain = Utils.load_chain_from_file("json-input-chains/nine-link-grid.json")

# distance constraints are linear distance constraints between edges
# and points of connected rigid bodies
distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(
    chain
)

# each node is given a human readable name
top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)

# construct a cost function for the linear program which moves the top right
# node of the chain as much as possible to the right
objective_vector = LinearFlexibilityAnalysis.construct_weight_vector(
    chain,
    [
        (top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0))
    ]
)

# repeatedly solves the linear program constructed from
# distance_constraints and objective_vector
# while limiting the error introduced to below maximum_error
LinearFlexibilityAnalysis.timestep_until_convergence!(
    chain,
    distance_constraints,
    maximum_error,
    objective_vector
)
```
