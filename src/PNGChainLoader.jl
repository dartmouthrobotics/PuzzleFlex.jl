module PNGChainLoader

import Images

import ..RigidBodyChains
import ..ConnectableRigidBodies


function get_bottom_left_pixel_of_color(pixels, color)
    right_colored_pixels = findall(pixel -> pixel == color, pixels)

    sorted_by_height = sort(
        right_colored_pixels,
        by=pixel -> to_global_coordinates(pixels, pixel)
    )

    sorted_by_height[1]
end


function get_neighboring_coordinates(coordinates)
    [
        CartesianIndex(coordinates[1], coordinates[2] + 1),
        CartesianIndex(coordinates[1] + 1, coordinates[2]),
        CartesianIndex(coordinates[1] - 1, coordinates[2]),
        CartesianIndex(coordinates[1], coordinates[2] - 1),
    ]
end


function is_in_bounds(coordinates, array)
    as_tuple = (coordinates[1], coordinates[2])

    minimum_coordinates = (1, 1)
    maximum_coordinates = size(array)

    all(
        map(
            bounds -> bounds[1] <= bounds[2] <= bounds[3],
            zip(
                minimum_coordinates,
                as_tuple,
                maximum_coordinates
            )
        )
    )
end


function get_filled_in_neighbors(pixels, coordinates, filled_color)
    filter(
        raw_neighbor -> is_in_bounds(
            raw_neighbor,
            pixels
        ) && pixels[raw_neighbor] == filled_color,
        get_neighboring_coordinates(coordinates)
    )
end


function get_required_body_type(pixels, coordinate::CartesianIndex, connectivity_graph::Dict, block_color::Images.RGB)
    neighboring_coordinates = get_filled_in_neighbors(pixels, coordinate, block_color)

    neighbor_types = []

    for neighbor in neighboring_coordinates
        neighbor_name = get_node_name_from_coordinates(pixels, neighbor)
        if haskey(connectivity_graph, neighbor_name)
            push!(neighbor_types, connectivity_graph[neighbor_name]["body_name"])
        end
    end
    
    if length(neighbor_types) == 0
        error("Cannot get required type for pixel. No neighboring pixels to $coordinate are constructed.")
    end

    unique_pixel_types = unique(neighbor_types)

    if length(unique_pixel_types) != 1
        error("Cannot get required type for pixel. More than one type of neighbor has been constructed. Cannot continue!")
    end

    if unique_pixel_types[1] == "20mm-female-four-sided-square"
        return "20mm-male-four-sided-square"
    end

    "20mm-female-four-sided-square"
end


function get_configuration_dict(pixels, pixel_index)
    global_coordinates = to_global_coordinates(pixels, pixel_index)
    block_side_length = 20.10

    Dict(
        "x" => global_coordinates[1] * block_side_length,
        "y" => global_coordinates[2] * block_side_length,
        "theta" => 0.0
    )
end


function to_global_coordinates(pixels, pixel_coordinates)
    (
        pixel_coordinates[2] - 1,
        size(pixels)[1] - pixel_coordinates[1]
    )
end


function get_node_name_from_coordinates(pixels, pixel_coordinates::CartesianIndex)
    "node($(pixel_coordinates[1] - 1),$(size(pixels)[2] - pixel_coordinates[2]))"
end


function construct_block_dict_from_pixel(pixels, pixel_coordinates, block_pixel_color, block_type)
    neighbor_coordinates = get_filled_in_neighbors(
        pixels,
        pixel_coordinates,
        block_pixel_color
    )

    Dict(
        "body_name" => block_type,
        "configuration" => get_configuration_dict(pixels, pixel_coordinates),
        "edges" => [
            Dict(
                "target" => get_node_name_from_coordinates(
                    pixels,
                    neighbor
               )
            ) for neighbor in neighbor_coordinates
        ]
    )
end


function construct_connectivity_graph(
    pixels,
    block_color::Images.RGB,
    fixed_pixel_index,
    fixed_block_type::String,
    female_name::String,
    male_name::String
)
    fixed_block_as_dict = construct_block_dict_from_pixel(
        pixels,
        fixed_pixel_index,
        block_color,
        fixed_block_type
    )

    connectivity_graph = Dict(
       get_node_name_from_coordinates(pixels, fixed_pixel_index) => fixed_block_as_dict
    )

    exploration_queue = [fixed_pixel_index]
    already_enqueued = [fixed_pixel_index]

    while length(exploration_queue) > 0
        current_pixel_coordinates = pop!(exploration_queue)

        pixel_node_dict = Dict()
        neighbor_coordinates = get_filled_in_neighbors(
            pixels,
            current_pixel_coordinates,
            block_color
        )

        for neighbor in neighbor_coordinates
            if !in(neighbor, already_enqueued)
                push!(exploration_queue, neighbor)
                push!(already_enqueued, neighbor)
            end
        end

        if current_pixel_coordinates != fixed_pixel_index
            current_body_type = get_required_body_type(
                pixels,
                current_pixel_coordinates,
                connectivity_graph,
                block_color
            )

            pixel_node_dict = construct_block_dict_from_pixel(
                pixels,
                current_pixel_coordinates,
                block_color,
                current_body_type
            )

            pixel_node_name = get_node_name_from_coordinates(
                pixels,
                current_pixel_coordinates,
            )

            connectivity_graph[pixel_node_name] = pixel_node_dict
        end
    end

    connectivity_graph
end


function load(image_path::String)
    male_path = "json-input-chains/json-chain-links/20mm-male-four-sided-square.json"
    female_path = "json-input-chains/json-chain-links/20mm-female-four-sided-square.json"

    male_body_name = "20mm-male-four-sided-square"
    female_body_name = "20mm-female-four-sided-square"

    image_pixels = Images.FileIO.load(image_path) 
    black = Images.RGB{Images.N0f8}(0.0, 0.0, 0.0)

    bottom_left_pixel_coordinates = get_bottom_left_pixel_of_color(image_pixels, black) 

    # make the dictionary with edges and nodes
    chain_dict = Dict("RigidBodyChain" => Dict())
    chain_dict["RigidBodyChain"]["RigidBodyTypes"] = [
        Dict("path" => male_path),
        Dict("path" => female_path)
    ]

    chain_dict["RigidBodyChain"]["ConnectivityGraph"] = construct_connectivity_graph(
        image_pixels,
        black,
        bottom_left_pixel_coordinates,
        "20mm-female-four-sided-square",
        "20mm-female-four-sided-square",
        "20mm-male-four-sided-square",
    )

    chain_dict["RigidBodyChain"]["FixedRigidBodyNode"] = get_node_name_from_coordinates(
        image_pixels,
        bottom_left_pixel_coordinates
    )

    RigidBodyChains.RigidBodyChain(chain_dict)
end

end
