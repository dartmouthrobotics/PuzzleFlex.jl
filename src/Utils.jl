module Utils

using ArgParse
import JSON

import ..PNGChainLoader
import ..RigidBodyChains

function get_parsed_args(args)
    argument_parser_settings = ArgParseSettings()
    @add_arg_table argument_parser_settings begin
        "-i", "--input-chain"
            help = "A JSON format description of a chain of loosely connected rigid bodies. See the directory json_input_chains for examples."
            required = true
    end

    parse_args(args, argument_parser_settings)
end


function load_chain_from_file(file_path::String)
    if endswith(file_path, "png")
        return PNGChainLoader.load(file_path)
    elseif endswith(file_path, "json")
        input_file_handle = open(file_path)
        json_contents = JSON.parse(read(input_file_handle, String))
        chain = RigidBodyChains.RigidBodyChain(json_contents)

        return chain
    else
        error("Unrecognized file type. The file path must end with json or png")
    end
end

end
