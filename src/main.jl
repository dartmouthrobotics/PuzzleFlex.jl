#! /usr/local/bin/julia --project
include("../examples/all_examples.jl")
using ArgParse
using StringDistances

function get_parsed_args(args)
    argument_parser_settings = ArgParseSettings()
    @add_arg_table argument_parser_settings begin
        "-e", "--example"
            help = "Select example(s) you want to run. "
            nargs = '+'
            required = false
    end

    parse_args(args, argument_parser_settings)
end

function main(args)
    parsed_args = get_parsed_args(args)
    if "example" in keys(parsed_args)
        for fn in parsed_args["example"]
            if fn in example_functions
                println("CALLING EXAMPLE ", fn)
                call_example_function(fn)()
            else
                println("INCORRECT EXAMPLE ", fn)
                println("DO YOU MEAN: ")
                candidate_examples = [f for f in example_functions if compare(Jaro(), f, fn) > 0.8]
                if length(candidate_examples) > 0
                    for f in candidate_examples
                        println(lpad("", 13, " "), f)
                    end
                else
                    for f in example_functions
                        println("", 13, " ", f)
                    end
                end
            end
        end
    else
        for fn in example_functions
            println("CALLING EXAMPLE ", fn)
            call_example_function(fn)()
        end
    end
end

main(ARGS)
