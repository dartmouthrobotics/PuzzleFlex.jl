"""
TESTING
"""
module RigidBodies

import GeometryTypes
import Clipper

const INT_POINT_CONVERSION_DECIMAL_PLACES = 6

struct RigidBody
    """
    The boundary should be specified with in a local coordinate frame near 
    the origin for this to make sense.
    """
    boundary::Vector{GeometryTypes.Point2f0}
end # struct RigidBody


function float_to_int(number::Real, decimal_places)
    trunc(Int64, number * (10.0^decimal_places))
end


function int_to_float(number::Integer, decimal_places)
    Float64(number) / (10^decimal_places)
end


function to_int_point(point::GeometryTypes.Point2f0, decimal_places)
    Clipper.IntPoint(
        float_to_int(point[1], decimal_places),
        float_to_int(point[2], decimal_places)
    )
end


function from_int_point(point::Clipper.IntPoint, decimal_places)
    GeometryTypes.Point2f0(
        int_to_float(point.X, decimal_places),
        int_to_float(point.Y, decimal_places)
    )
end


function convert_boundary_to_clipper_path(rigid_body::RigidBody, decimal_places)
    map(
        to_int_point,
        rigid_body.boundary,
        repeat([decimal_places], length(rigid_body.boundary))
    )
end


function convert_clipper_path_to_float_points(path, decimal_places)
    map(
        from_int_point,
        path,
        repeat([decimal_places], length(path))
    )
end


function get_offset_boundary(rigid_body::RigidBody, offset_amount)
    """
    Offsets the boundary of the given rigid body by an amount equal to offset_amount
    """

    offsetter = Clipper.ClipperOffset()
    boundary_as_clipper_path = convert_boundary_to_clipper_path(
        rigid_body,
        INT_POINT_CONVERSION_DECIMAL_PLACES 
    )

    Clipper.add_path!(
        offsetter,
        boundary_as_clipper_path,
        Clipper.JoinTypeMiter,
        Clipper.EndTypeClosedPolygon
    )

    clipper_offsets = Clipper.execute(offsetter, offset_amount * 10^INT_POINT_CONVERSION_DECIMAL_PLACES)

    if length(clipper_offsets) > 1
        error("Offset distance is too high. The boundary of this rigid body was broken into many pieces")
    end

    resultant_boundary = convert_clipper_path_to_float_points(
        clipper_offsets[1],
        INT_POINT_CONVERSION_DECIMAL_PLACES 
    )

    push!(resultant_boundary, resultant_boundary[1])
    resultant_boundary
end


function get_rigid_body_with_offset_boundary(rigid_body::RigidBody, offset_amount)
    new_boundary = get_offset_boundary(rigid_body, offset_amount)
    RigidBody(new_boundary)
end


end # module RigidBodies
