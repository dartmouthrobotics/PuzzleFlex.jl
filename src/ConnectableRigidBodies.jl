
module ConnectableRigidBodies

import ..RigidBodies
import ..Geometry

const ConnectableRigidBodyID = String

"""
A rigid body which may connect to another rigid body
"""
struct ConnectableRigidBody
    # is a rigid body
    connectable_body_id::ConnectableRigidBodyID
    rigid_body::RigidBodies.RigidBody
end # struct ConnectableRigidBody


function load_from_json(body_type_json::Dict)
    boundary_points = map(Geometry.parse_point_from_json, body_type_json["boundary"])

    ConnectableRigidBody(
        body_type_json["name"],
        RigidBodies.RigidBody(
            boundary_points
        )
    )
end


end # module ConnectableRigidBodies
