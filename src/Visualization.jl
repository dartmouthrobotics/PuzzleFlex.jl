module Visualization

import Base.Iterators

using PyCall

import PyPlot
import JSON
import LightGraphs: vertices, edges, src, dst
import MetaGraphs: props
import GeometryTypes
import GeometryTypes: Point2f0
import CoordinateTransformations

import ..Geometry
import ..RigidBodyChains
import ..LinearFlexibilityAnalysis



const MINT_GREEN = (144.0 / 255.0, 238.0 / 255.0, 144.0 / 255.0)
const WARM_GREY = (211.0 / 255.0, 211.0 / 255.0, 211.0 / 255.0)
const BLACK = (0.0, 0.0, 0.0)
const RED = (1.0, 0.0, 0.0)
const YELLOW = (1.0, 1.0, 0.0)

const matplotlib_patches = PyNULL()
const matplotlib_collections = PyNULL()

function __init__()
    PyPlot.matplotlib[:use]("Qt5Agg")

    copy!(matplotlib_patches, pyimport_conda("matplotlib.patches", "matplotlib"))
    copy!(matplotlib_collections, pyimport_conda("matplotlib.collections", "matplotlib"))
end


function convert_polygon_to_matplotlib_polygon(boundary_points)
    boundary_as_array = reduce(vcat, map(transpose, boundary_points))

    matplotlib_patches[:Polygon](boundary_as_array)
end


function surround_chains!(axis, chains)
    boundaries = []
    for chain in chains
        for node in RigidBodyChains.get_all_node_ids(chain)
            boundary_points = RigidBodyChains.get_rigid_body_boundary(chain, node)
            boundary_as_array = reduce(vcat, map(transpose, boundary_points))
            push!(boundaries, boundary_as_array)
        end
    end

    all_points = reduce(vcat, boundaries)
    axis[:set_xlim]((minimum(all_points[:, 1]) - 1), maximum(all_points[:, 1]) + 1)
    axis[:set_ylim]((minimum(all_points[:, 2]) - 1), maximum(all_points[:, 2]) + 1)
end


"""
Used to fix RGB color to RGBA array, if color is correct then return original
"""
function check_color(color_tuple)
    l_c = length(color_tuple)

    if !(l_c == 3 || l_c == 4)
        error("Incorrect color format")
    end

    return color_tuple
end


"""
Accepts customized color for each block. To customize the fill color of a node, pass a Dict mapping node name to the color
"""
function display_chain_links!(axes, chain; fill_color=WARM_GREY, boundary_color=BLACK, boundary_width=1.0)

    link_boundaries_pair = map(
        link_node -> RigidBodyChains.get_node_name(chain, link_node) => reduce(vcat, map(transpose, RigidBodyChains.get_rigid_body_boundary(chain, link_node))),
        vertices(chain.connectivity_graph)
    )

    fill_color_dict = Dict()
    if isa(fill_color, AbstractDict)
        # Then it means some nodes color are customized
        if ! reduce(&, [haskey(fill_color, p.first) for p in link_boundaries_pair])
            @debug "Not all blocks color are given, some blocks will use WARM_GREY by default"
        end

        for p in link_boundaries_pair
            if haskey(fill_color, p.first)
                fill_color_dict[p.first] = check_color(fill_color[p.first])
            else
                fill_color_dict[p.first] = WARM_GREY
            end
        end

    else
        fill_color_dict = Dict(p.first => fill_color for p in link_boundaries_pair)
    end

    # This part is used to set scene and limit our scene size
    polygon_patches = []

    link_names = [p.first for p in link_boundaries_pair]
    link_boundaries = [p.second for p in link_boundaries_pair]

    for link_boundary in link_boundaries
        link_polygon = matplotlib_patches[:Polygon](link_boundary)
        push!(polygon_patches, link_polygon)
    end

    all_boundary_points = reduce(vcat, link_boundaries)

    y_max = maximum(all_boundary_points[:, 2])
    y_min = minimum(all_boundary_points[:, 2])
    x_max = maximum(all_boundary_points[:, 1])
    x_min = minimum(all_boundary_points[:, 1])
    axes[:set_xlim]((x_min, x_max))
    axes[:set_ylim]((y_min, y_max))

    patch_collection = matplotlib_collections[:PatchCollection](polygon_patches)

    if isa(fill_color, AbstractDict)
        fill_colors = [fill_color_dict[link_name] for link_name in link_names] 
        patch_collection[:set_color](fill_colors) 
    else
        patch_collection[:set_color](fill_color)
    end

    patch_collection[:set_edgecolor](boundary_color)
    patch_collection[:set_linewidth](boundary_width)

    axes[:add_collection](patch_collection)
end

function get_link_boundaries(chain, t)

    link_boundaries = map(
        vertex -> map(Point2f0, RigidBodyChains.get_rigid_body_boundary(chain, vertex)),
        vertices(chain.connectivity_graph)
    )

    return link_boundaries

end


function get_link_center_point(vertex, connectivity_graph)
    vertex_props = props(connectivity_graph, vertex)
    configuration = vertex_props[:rigid_body_configuration]

    local_to_global_map = Geometry.construct_frame_transform(configuration)

    origin = Point2f0(0)
    local_to_global_map(origin)
end


function display_connectivity_graph!(
    axis::PyCall.PyObject,
    chain::RigidBodyChains.RigidBodyChain;
    vertex_color=(0, 235.0 / 255.0, 239.0 / 255.0),
    edge_color=(0, 235.0 / 255.0, 239.0 / 255.0),
    vertex_size=5.0,
    edge_width=1.0
)
    edge_line_segments = []
    center_point_x_values = []
    center_point_y_values = []

    for edge in edges(chain.connectivity_graph)
        source_center = get_link_center_point(src(edge), chain.connectivity_graph)
        dest_center = get_link_center_point(dst(edge), chain.connectivity_graph)

        push!(edge_line_segments, [tuple(source_center...), tuple(dest_center...)])

        push!(center_point_x_values, source_center[1])
        push!(center_point_x_values, dest_center[1])

        push!(center_point_y_values, source_center[2])
        push!(center_point_y_values, dest_center[2])
    end

    edge_collection = matplotlib_collections[:LineCollection](edge_line_segments, linewidths=edge_width, colors=edge_color)
    axis[:scatter](center_point_x_values, center_point_y_values, s=vertex_size, color=vertex_color)
    axis[:add_collection](edge_collection)
end


function clear_axis_markers!(axis)
    PyPlot.box(on=nothing)
    axis[:get_xaxis]()[:set_visible](false)
    axis[:get_yaxis]()[:set_visible](false)
end


function find_center_points(chain, weight_vector, distance_constraints)
    center_points = Array{Point2f0, 1}()

    for edge in edges(chain.connectivity_graph)
        source_center = get_link_center_point(src(edge), chain.connectivity_graph)
        dest_center = get_link_center_point(dst(edge), chain.connectivity_graph)

        push!(center_points, (source_center[1], source_center[2]))
        push!(center_points, (dest_center[1], dest_center[2]))
    end

    return center_points
end

function find_edge_segments(chain)
    edge_line_segments = Array{Pair{Point2f0, Point2f0}, 1}()

    for edge in edges(chain.connectivity_graph)
        source_center = get_link_center_point(src(edge), chain.connectivity_graph)
        dest_center = get_link_center_point(dst(edge), chain.connectivity_graph)

        push!(edge_line_segments, source_center => dest_center)
    end

    return edge_line_segments
end

function display_distance_constraints!(
    axis,
    chain,
    constraints_by_binding_pair;
    point_color=(0.0, 1.0, 0.0),
    edge_color=(1.0, 0.0, 0.0),
    v_color=(0.0, 0.0, 1.0),
    point_size=1.0,
    edge_width=1.0,
    v_width=1.0
)
    constraint_point_x_values::Array{Float32} = []
    constraint_point_y_values::Array{Float32} = []

    edge_lines = []
    v_lines = []

    for constraints in constraints_by_binding_pair
        for constraint in constraints.second
            constraint_point = RigidBodyChains.get_rigid_body_boundary(chain, constraint.point_node)[constraint.point]
            constraint_edge = RigidBodyChains.get_rigid_body_boundary(chain, constraint.edge_node)[collect(constraint.edge)]

            push!(constraint_point_x_values, constraint_point[1])
            push!(constraint_point_y_values, constraint_point[2])
            push!(edge_lines, [(constraint_edge[1][1], constraint_edge[1][2]), (constraint_edge[2][1], constraint_edge[2][2])])

            v_dest = Geometry.get_segment_center_point(constraint_edge[1], constraint_edge[2])

            edge_center = Geometry.get_segment_center_point(constraint_edge[1], constraint_edge[2])
            push!(v_lines, [(constraint_point[1], constraint_point[2]), (edge_center[1], edge_center[2])])
        end
    end

    edges_collection = matplotlib_collections[:LineCollection](edge_lines, linewidths=edge_width, colors=edge_color)
    v_collection = matplotlib_collections[:LineCollection](v_lines, linewidths=v_width, colors=v_color)

    axis[:add_collection](edges_collection)
    axis[:add_collection](v_collection)

    axis[:scatter](constraint_point_x_values, constraint_point_y_values, color=point_color, s=point_size)
end


function interpolate_color(start_color, end_color, interpolation_amount)
    (end_color - start_color) * interpolation_amount + start_color
end


function display_robot_flock!(axis, robot_flock; alpha=0.4)

    field_of_view_patches = []
    body_patches = []
    transmitter_patches = []

    transmitter_radius = 0.1

    node_ids = RigidBodyChains.get_all_node_ids(robot_flock)

    for node_id in node_ids
        node_boundary = RigidBodyChains.get_rigid_body_boundary(
            robot_flock,
            node_id
        )

        node_name = RigidBodyChains.get_node_name(robot_flock, node_id)
        node_configuration = RigidBodyChains.get_configuration(
            robot_flock,
            node_id
        )

        frame_transform = Geometry.construct_frame_transform(
            node_configuration
        )

        origin_in_frame = frame_transform([0, 0])

        if occursin("node", node_name)
            triangle = node_boundary[7:end]

            square_body_in_frame = convert_polygon_to_matplotlib_polygon(node_boundary[2:6])

            push!(field_of_view_patches, convert_polygon_to_matplotlib_polygon(triangle))
            push!(body_patches, square_body_in_frame)
            push!(transmitter_patches, matplotlib_patches[:Circle](origin_in_frame, transmitter_radius, fill=true))
        elseif occursin("rogue-commander", node_name)
            # draw only the body
            square_body_in_frame = convert_polygon_to_matplotlib_polygon(node_boundary[2:6])

            push!(body_patches, square_body_in_frame)
            push!(transmitter_patches, matplotlib_patches[:Circle](origin_in_frame, transmitter_radius, fill=true))
        end
    end

    field_of_view_collection = matplotlib_collections[:PatchCollection](field_of_view_patches)
    field_of_view_collection[:set_color]((YELLOW..., alpha))
    field_of_view_collection[:set_edgecolor]((BLACK..., alpha))
    field_of_view_collection[:set_linewidth](0.1)

    body_collection = matplotlib_collections[:PatchCollection](body_patches)
    body_collection[:set_color]((WARM_GREY..., alpha))
    body_collection[:set_edgecolor]((BLACK..., alpha))
    body_collection[:set_linewidth](0.1)

    transmitter_collection = matplotlib_collections[:PatchCollection](transmitter_patches)
    transmitter_collection[:set_color]((RED..., alpha))

    axis[:set_xlim]((-200, 200))
    axis[:set_ylim]((-400, 60))

    axis[:add_collection](body_collection)
    axis[:add_collection](transmitter_collection)
    axis[:add_collection](field_of_view_collection)
end


"""
    save_with_border(filename::String, chain::RigidBodyChains.RigidBodyChain; fill_color_dict=Visualization.WARM_GREY, save_figure=true, save_configuration=true)

save current chain to figure and configuration. filename should end with "pdf" or image format suffix.
fill_color can be used to set up color for each node in chain. If you want to save figure or configuration file (JSON) based on your requirement, you can switch
save_figure and save_configuration.
"""
function save_with_border(filename::String, chain::RigidBodyChains.RigidBodyChain; fill_color_dict=Visualization.WARM_GREY)
    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(10, 10))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)
    Visualization.display_chain_links!(axis, chain; fill_color=fill_color_dict)
    edge_width = 4.0
    boundary_color = (0, 0, 0)
    border_lines = []
    x_min = -15
    x_max = 55
    y_min = -35
    y_max = 35
    push!(border_lines, (x_min, y_max) => (x_min, y_min))
    push!(border_lines, (x_min, y_min) => (x_max, y_min))
    push!(border_lines, (x_max, y_min) => (x_max, y_max))
    push!(border_lines, (x_max, y_max) => (x_min, y_max))
    patch_collection_border = matplotlib_collections[:LineCollection](border_lines, linewidths=edge_width, colors = boundary_color)
    axis[:add_collection](patch_collection_border)
    axis[:set_xlim]((x_min, x_max))
    axis[:set_ylim]((y_min, y_max))
    figure[:savefig](filename, bbox_inches="tight", dpi=400)
end


"""
    save_configuration_to_json(file_name, chain)

Saves the configuration of the rigid body chain to JSON.
"""
function save_configuration_to_json(file_name, chain)
    node_configuration = Dict((RigidBodyChains.get_node_name(chain, node_id) =>
        Dict(
              "old_boundary" => RigidBodyChains.get_rigid_body_boundary(
                  chain,
                  node_id,
                  apply_configuration_map=false
              ),
              "new_boundary" => RigidBodyChains.get_rigid_body_boundary(
                  chain,
                  node_id
              ),
              "type" => RigidBodyChains.get_node_type(
                  chain,
                  node_id
              ),
              "configuration" => RigidBodyChains.get_configuration(
                  chain,
                  node_id
              )
        )
        for node_id in RigidBodyChains.get_all_node_ids(chain))
    )
        
    open(file_name, "w") do f
        JSON.print(f, node_configuration)
    end
end

end # module
