module Geometry

import LinearAlgebra
import Base.Iterators: take, drop

import GeometryTypes
import Distributions
import CoordinateTransformations


function construct_random_rotation2D(min_allowed::Real, max_allowed::Real)
    rand(Distributions.Uniform(min_allowed, max_allowed))
end


function construct_random_translation2D(min_allowed, max_allowed)
    GeometryTypes.Vec2f0(rand(Distributions.Uniform(min_allowed, max_allowed), 2, 1))
end


function construct_random_point2D(x_range, y_range)
    GeometryTypes.Point2f0(
        rand(Distributions.Uniform(x_range...)),
        rand(Distributions.Uniform(y_range...))
    )
end # construct_random_point2D


function construct_random_transformation2D(rotation_range, translation_range, rotation_center_bounds)
    construct_homogenous_transformation_matrix(
        construct_random_point2D(rotation_center_bounds...),
        construct_random_rotation2D(rotation_range...),
        construct_random_translation2D(translation_range...),
    )
end


function construct_translation_matrix(x, y)
    [
        1 0 x;
        0 1 y;
        0 0 1
    ]
end


function construct_rotation_matrix(theta)
    [
         cos(theta) -sin(theta) 0;
         sin(theta) cos(theta) 0;
         0 0 1
    ]
end


function to_homogenous(point::GeometryTypes.Point)
    [point[1]; point[2]; 1]
end


function from_homogenous(point::GeometryTypes.Point)
    GeometryTypes.Point2f0(point[1], point[2])
end


function rotation_about_point(rotation_center, theta)
    construct_translation_matrix(rotation_center...) * construct_rotation_matrix(theta) * construct_translation_matrix((-1*rotation_center...)) 
end


function construct_homogenous_transformation_matrix(theta, rotation_center, translation)
    construct_translation_matrix(translation...) * rotation_about_point(rotation_center, theta)
end


function configuration_to_workspace_transformation_matrix(configuration)
    construct_rotation_matrix(configuration[3]) * construct_translation_matrix(configuration[1], configuration[2])
end


function iterate_edges(polygon)
    return zip(
        take(polygon, length(polygon) - 1),
        drop(polygon, 1)
    )
end


function triangles_collide(a, b)
    any(flatten([[polygon_contains_point(b, a_vertex) for a_vertex in a], [polygon_contains_point(a, b_vertex) for b_vertex in b]]))
end


function random_configuration(bounding_box)
    GeometryTypes.Point3f0(
        rand(Distributions.Uniform(bounding_box[1][1], bounding_box[1][2])),
        rand(Distributions.Uniform(bounding_box[2][1], bounding_box[2][2])),
        rand(Distributions.Uniform(bounding_box[3][1], bounding_box[3][2]))
    )
end


function construct_rotation_coordinate_transform(theta)
    CoordinateTransformations.LinearMap([ cos(theta) -sin(theta); sin(theta) cos(theta) ])
end


function construct_frame_transform(configuration)
    translation = CoordinateTransformations.Translation(configuration[[1, 2]])
    theta = configuration[3]
    rotation_map = construct_rotation_coordinate_transform(theta)

    CoordinateTransformations.compose(translation, rotation_map)
end


function inverse_3D_rect()
    infinitely_small = GeometryTypes.Vec3f0(-Inf32)
    infinitely_large = GeometryTypes.Vec3f0(Inf32)

    GeometryTypes.HyperRectangle{3, Float32}(infinitely_large, infinitely_small)
end


function min_max_corner(rect)
    (rect.origin, rect.origin + rect.widths)
end


function is_infinitely_small_rect(rect)
    any(map(isinf, rect.origin))
end


function expand_3D_rect_by_point(rect, point)
    if is_infinitely_small_rect(rect)
        return GeometryTypes.HyperRectangle(GeometryTypes.Vec3f0(point...), GeometryTypes.Vec3f0(0)) 
    end

    bottom_left, top_right = min_max_corner(rect)

    new_bottom_left = GeometryTypes.Vec3f0(map(minimum, zip(bottom_left, point)))
    new_top_right = GeometryTypes.Vec3f0(map(maximum, zip(top_right, point)))

    GeometryTypes.HyperRectangle(new_bottom_left, new_top_right - new_bottom_left)
end


function construct_bounding_rect_of_3D_points(points)
    result = inverse_3D_rect()
    for point in points
        result = expand_3D_rect_by_point(result, point)
    end

    result
end


function get_corners(rect)
    origin = rect.origin
    widths = rect.widths

    combinations = zip(widths, [0.0, 0.0, 0.0])
    offsets = Iterators.product(combinations...)

    corners = [
       origin + GeometryTypes.Point3f0(offset) for offset in offsets
    ]

    corners
end


"""
"""
function iterate_as_grid(rect, grid_resolutions) 
    min_corner, max_corner = min_max_corner(rect)

    if any(map(isnan, min_corner)) || any(map(isnan, max_corner))
        error(
            "Got a NaN in the min or max corner of the given rect during iterate_as_grid. rect: $(rect)"
        )
    end

    coordinate_ranges = map(
        coordinate_min_max -> coordinate_min_max[1]:coordinate_min_max[3]:coordinate_min_max[2],
        zip(min_corner, max_corner, grid_resolutions)
    )

    Iterators.product(
        coordinate_ranges...
    )
end


function parse_point_from_json(point_json::Dict)
    GeometryTypes.Point2f0(point_json["x"], point_json["y"])
end


function distance_to_line_segment(line_segment, point)
    # project point onto line then 
    # taken from https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
    
    l2 = sum((line_segment[2] - line_segment[1]) .^ 2)

    t = max(
        0,
        min(
            1,
            LinearAlgebra.dot(point - line_segment[1], line_segment[2] - line_segment[1]) / l2
        )
    );

    projection = line_segment[1] + t * (line_segment[2] - line_segment[1]);

    LinearAlgebra.norm(point - projection)
end


function get_segment_center_point(source, dest)
    segment_direction = dest - source
    mid_point = source + 0.5 * segment_direction
    mid_point
end


function orientation2D(a, b, c)
    x1, y1 = a
    x2, y2 = b
    x3, y3 = c

    orientation_sign = (y2 - y1)*(x3 - x2) - (y3 - y2)*(x2 - x1)

    orientation_sign
end


function share_endpoint(seg_a, seg_b)
    in(seg_a[1], seg_b) || in(seg_a[2], seg_b)
end


function segments_intersect(seg_a, seg_b)
    # returns false if the segments do share an endpoint
    # and if they properly intersect
    a1, a2 = seg_a
    b1, b2 = seg_b

    if share_endpoint(seg_a, seg_b)
        return false
    end

    o1 = orientation2D(a1, a2, b1)
    o2 = orientation2D(a1, a2, b2)

    o3 = orientation2D(b1, b2, a1)
    o4 = orientation2D(b1, b2, a2)

    first_orientation_pair_different = (o1 < 0 && o2 > 0) || (o1 > 0 && o2 < 0)
    second_orientation_pair_different = (o3 < 0 && o4 > 0) || (o3 > 0 && o4 < 0)

    intersection_result = first_orientation_pair_different && second_orientation_pair_different

    intersection_result
end


end # module
