"""
Translates between `RigidBodyChain`s and the linear programming formulation
"""
module LinearFlexibilityAnalysis

import LinearAlgebra
import Base.Iterators
import SparseArrays

import ForwardDiff
import GeometryTypes
import Convex
import Clp
import SCS
import DataStructures

USE_GUROBI = get(ENV, "GUROBI_HOME", nothing) != nothing

if USE_GUROBI
    import Gurobi
end

import ..RigidBodyChains
import ..BindingPairs
import ..Geometry

const RADIANS_TO_ROTATION_UNITS = 180.0 / pi
const ROTATION_UNITS_TO_RADIANS = 1.0 / RADIANS_TO_ROTATION_UNITS

const MAX_LINEARIZED_DISTANCE_FOR_CONSTRAINT = 3.0
const MAX_REAL_EDGE_DISTANCE_FOR_CONSTRAINT = 3.0


"""
A model of the requirement that a point on the given point_node
"""
struct DistanceConstraint
    edge_node::Real
    point_node::Real
    edge::Tuple{Real, Real}
    point::Real
end


"""
    distance_to_edge(chain, constraint)

Returns how far the point in the constraint is from the edge in the same constraint using a real
(ie quadratic rather than linear) distance.
"""
function distance_to_edge(chain, constraint)
    distance_to_edge_along_normal = edge_point_linearized_signed_distance(
        chain,
        constraint
    )

    edge_node_boundary = RigidBodyChains.get_rigid_body_boundary(chain, constraint.edge_node)
    point_node_boundary = RigidBodyChains.get_rigid_body_boundary(chain, constraint.point_node)
    edge_source = edge_node_boundary[constraint.edge[1]]
    edge_target = edge_node_boundary[constraint.edge[2]]

    point = point_node_boundary[constraint.point]

    Geometry.distance_to_line_segment(edge_source => edge_target, point)
end


"""
    select_edge_and_point_node(chain::RigidBodyChains.RigidBodyChain, binding_pair::BindingPairs.BindingPair)

Selects an edge and point node by choosing the socket node to be the edge node and the pin node to be the
point node. In future work, we will improve this algorithm

returns (edge_node, point_node)
"""
function select_edge_and_point_node(chain::RigidBodyChains.RigidBodyChain, binding_pair::BindingPairs.BindingPair)

    source_body_type = binding_pair.base_body_type
    target_body_type = binding_pair.relative_body_type

    source_node_id = binding_pair.from_node_id
    target_node_id = binding_pair.to_node_id

    if source_body_type == "20mm-male-four-sided-square" && target_body_type == "20mm-female-four-sided-square"
        return (target_node_id, source_node_id)
    end

    (source_node_id, target_node_id)
end


function construct_raw_distance_constraints(
    binding_pair::BindingPairs.BindingPair,
    chain::RigidBodyChains.RigidBodyChain
)

    edge_node, point_node = select_edge_and_point_node(chain, binding_pair)

    point_node_boundary_size = length(
        RigidBodyChains.get_rigid_body_boundary(
            chain,
            point_node
        )
    )

    edge_node_boundary_size = length(
        RigidBodyChains.get_rigid_body_boundary(
            chain,
            edge_node
        )
    )
    edge_boundary_indices = push!(collect(1:edge_node_boundary_size))
    edges = Geometry.iterate_edges(edge_boundary_indices)
    points = collect(1:point_node_boundary_size)

    getfirst(x) = getindex(x, 1)
    getsecond(x) = getindex(x, 2)

    constraint_edges = map(getfirst, Iterators.product(edges, points))
    constraint_points = map(getsecond, Iterators.product(edges, points))

    raw_distance_constraints = map(
        DistanceConstraint,
        repeat([edge_node], length(constraint_edges)),
        repeat([point_node], length(constraint_points)),
        constraint_edges,
        constraint_points,
    )

    raw_distance_constraints

end


function construct_distance_constraints(
    binding_pair::BindingPairs.BindingPair,
    chain::RigidBodyChains.RigidBodyChain
)
    raw_distance_constraints = construct_raw_distance_constraints(binding_pair, chain)

    non_degenerate_constraints = filter(
        constraint -> constraint.edge[1] != constraint.edge[2],
        raw_distance_constraints
    )

    large_enough_constraints = filter(
        constraint -> 0.001 <= distance_to_edge(chain, constraint) <= MAX_REAL_EDGE_DISTANCE_FOR_CONSTRAINT && 0.001 <= edge_point_linearized_signed_distance(chain, constraint) <= MAX_LINEARIZED_DISTANCE_FOR_CONSTRAINT,
        non_degenerate_constraints
    )

    # filter constraints where there are two points on the same edge point to the same edge
    # if the line from the point to the midpoint of the edge crosses an edge of the point node
    noncrossing_constraints = filter(
        constraint -> constraint_does_not_cross_edge(chain, constraint),
        large_enough_constraints
    )
    
    noncrossing_constraints
end

function constraint_does_not_cross_edge(chain, constraint)
    edge_node_boundary = RigidBodyChains.get_rigid_body_boundary(chain, constraint.edge_node)
    point_node_boundary = RigidBodyChains.get_rigid_body_boundary(chain, constraint.point_node)

    edge = edge_node_boundary[collect(constraint.edge)]
    point = point_node_boundary[constraint.point]

    edge_center_point = Geometry.get_segment_center_point(edge...)
    edge_point_connector = (edge_center_point, point)

    point_node_edges = Geometry.iterate_edges(point_node_boundary)

    !any(
        map(
            Geometry.segments_intersect,
            point_node_edges,
            repeat([edge_point_connector], length(point_node_edges))
        )
    )
end

function construct_distance_constraints(chain::RigidBodyChains.RigidBodyChain)
    binding_pairs = RigidBodyChains.get_all_forward_binding_pairs(chain)

    distance_constraints_by_binding_pair = DataStructures.SortedDict()

    for binding_pair in binding_pairs
        distance_constraints_by_binding_pair[binding_pair.binding_pair_id] = construct_distance_constraints(
            binding_pair,
            chain
        )
    end

    distance_constraints_by_binding_pair
end


function get_configuration(chain, left_node, right_node)
    result = Array(vcat(
        RigidBodyChains.get_configuration(chain, left_node),
        RigidBodyChains.get_configuration(chain, right_node),
   ))

    result
end


function get_partial_derivatives_of_distance_function(chain, point_node, edge_node, edge_idx, point_idx)
    # want six values: change in distance wrt point_node xytheta
    # change in distance wrt edge node xytheta
    full_configuration = get_configuration(chain, point_node, edge_node)
    as_function_of_configuration = get_signed_distance_as_function_of_configuration(
        chain,
        edge_node,
        point_node,
        edge_idx,
        point_idx
    )

    result = ForwardDiff.gradient(
        as_function_of_configuration,
        full_configuration
    )

    result
end


function get_outward_normal(edge_points)
    edge_direction = edge_points[2] - edge_points[1]
    LinearAlgebra.normalize(
        [edge_direction[2], -edge_direction[1]]
    )
end


function signed_distance(
    node_configurations,
    point_distance,
    point_internal_angle,
    edge_source_internal_angle,
    edge_target_internal_angle,
    edge_source_distance,
    edge_target_distance;
    logging = false
)
    """
    This is written to be usable by automatic differentiation because the full
    set of six partial derivatives is too tedious to write down

    distance = nx * vx + ny * vy

    vx = px - e0x
    vy = py - e0y

    px = node_configurations[1] + point_distance * cos(point_internal_angle + node_configurations[3])
    py = node_configurations[2] + point_distance * sin(point_internal_angle + node_configurations[3])

    nx = [ky, -kx]

    kx = e1x - e0x
    ky = e1y - e0y

    e0x = node_configurations[4] + edge_source_distance * cos(edge_source_internal_angle + node_configurations[6])
    e0y = node_configurations[5] + edge_source_distance * sin(edge_source_internal_angle + node_configurations[6])

    e1x = node_configurations[4] + edge_target_distance * cos(edge_target_internal_angle + node_configurations[6])
    e1y = node_configurations[5] + edge_target_distance * sin(edge_target_internal_angle + node_configurations[6])
    """

    theta0 = node_configurations[6]
    theta1 = node_configurations[3]

    edge_node_x = node_configurations[4]
    edge_node_y = node_configurations[5]

    e0x = edge_node_x + (edge_source_distance * cos(edge_source_internal_angle + theta0))
    e0y = edge_node_y + (edge_source_distance * sin(edge_source_internal_angle + theta0))
    e1x = edge_node_x + (edge_target_distance * cos(edge_target_internal_angle + theta0))
    e1y = edge_node_y + (edge_target_distance * sin(edge_target_internal_angle + theta0))

    px = node_configurations[1] + (point_distance * cos(point_internal_angle + theta1))
    py = node_configurations[2] + (point_distance * sin(point_internal_angle + theta1))

    vx = px - e0x
    vy = py - e0y

    kx = e1x - e0x
    ky = e1y - e0y


    k_norm = sqrt(kx ^ 2 + ky ^ 2)

    nx = ky / k_norm
    ny = -kx / k_norm

    if typeof(nx) == Float32 && logging
        println("~~~")
        println("PDIST * sin theta ", point_distance * cos(point_internal_angle + theta1))
        println("EDGE ", e0x, ", ", e0y, " => ", e1x, ", ", e1y)
        println("NORMAL ", nx, ", ", ny)
        println("POINT ", px, ", ", py)
        println("RESULT ", nx * vx + ny * vy)
        println("FULL CFG ", node_configurations)
    end

    if isnan(nx)
        error("Signed distance computation failed! Unable to continue with NaN normal coordinate.")
    end

    nx * vx + ny * vy
end

function get_signed_distance_as_function_of_configuration(chain, edge_node, point_node, edge_indices, point_index)
    point_node_configuration = RigidBodyChains.get_configuration(chain, point_node)
    edge_node_configuration = RigidBodyChains.get_configuration(chain, edge_node)

    edge_node_boundary = RigidBodyChains.get_rigid_body_boundary(
        chain,
        edge_node,
        apply_configuration_map=false
    )

    point_location = RigidBodyChains.get_rigid_body_boundary(
        chain,
        point_node,
        apply_configuration_map=false
    )[point_index]

    edge_source_location = edge_node_boundary[edge_indices[1]]
    edge_target_location = edge_node_boundary[edge_indices[2]]

    point_distance = LinearAlgebra.norm(point_location)
    edge_source_distance = LinearAlgebra.norm(edge_source_location)
    edge_target_distance = LinearAlgebra.norm(edge_target_location)

    point_internal_angle = atan((point_location)[[2, 1]]...)
    edge_source_internal_angle = atan((edge_source_location)[[2, 1]]...)
    edge_target_internal_angle = atan((edge_target_location)[[2, 1]]...)

    as_function_of_configuration(configuration::Vector) = signed_distance(
        configuration,
        point_distance,
        point_internal_angle,
        edge_source_internal_angle,
        edge_target_internal_angle,
        edge_source_distance,
        edge_target_distance,
        logging = false
    )

    as_function_of_configuration
end


function edge_point_linearized_signed_distance(chain::RigidBodyChains.RigidBodyChain, distance_constraint::DistanceConstraint)
    full_configuration = get_configuration(chain, distance_constraint.point_node, distance_constraint.edge_node)

    signed_distance_function = get_signed_distance_as_function_of_configuration(
        chain,
        distance_constraint.edge_node,
        distance_constraint.point_node,
        distance_constraint.edge,
        distance_constraint.point
    )
    # Used to debug boundary problem
    result = nothing
    try
        result = signed_distance_function(full_configuration)
    catch e
        println(RigidBodyChains.get_node_name(chain, distance_constraint.edge_node),"----",
        RigidBodyChains.get_node_name(chain, distance_constraint.point_node),"----",
        distance_constraint.edge,"----",
        distance_constraint.point)
        throw(e)
    end
    result
end


function get_configuration_variable_indices(chain, node_id)
    if node_id == chain.fixed_rigid_body_node_id
        return []
    end

    variable_offset = ((node_id - 1) * 3) + 1
    if node_id > chain.fixed_rigid_body_node_id
        variable_offset = ((node_id - 2) * 3) + 1
    end

    return [variable_offset, variable_offset + 1, variable_offset + 2]
end


function get_number_of_variables(chain::RigidBodyChains.RigidBodyChain)
    (RigidBodyChains.get_number_of_rigid_bodies(chain) - 1) * 3
end


function construct_constraint_jacobian_row(
    chain,
    point_node,
    edge_node,
    distance_constraint
)
    """
    Get the change in the distance function with the change in node_a's configuration.

    How do we handle change in node b's configuration's effect on this distance. Each
    row is a single distance constraint, so the whole construction of the jacobian given
    the points and edges
    """

    jacobian_row = zeros(1, get_number_of_variables(chain))
    point_node_variables = get_configuration_variable_indices(chain, point_node)
    edge_node_variables = get_configuration_variable_indices(chain, edge_node)
    edge_indices = distance_constraint.edge
    point_index = distance_constraint.point

    partial_derivatives = get_partial_derivatives_of_distance_function(
        chain,
        point_node,
        edge_node,
        edge_indices,
        point_index
    )

    all_variable_indices = vcat(point_node_variables, edge_node_variables)

    for (partial_index, jacobian_column) in enumerate(point_node_variables)
        jacobian_row[jacobian_column] = partial_derivatives[partial_index]
    end

    for (partial_index, jacobian_column) in enumerate(edge_node_variables)
        jacobian_row[jacobian_column] = partial_derivatives[partial_index + 3]
    end

    jacobian_row
end


# points on node_a, points on node_b
function construct_constraint_jacobian_rows(
    chain::RigidBodyChains.RigidBodyChain,
    binding_pair_id::BindingPairs.BindingPairID,
    distance_constraints_by_binding_pair
)
    distance_constraints = distance_constraints_by_binding_pair[binding_pair_id]

    [
        construct_constraint_jacobian_row(
            chain,
            constraint.point_node,
            constraint.edge_node,
            constraint
        ) for constraint in distance_constraints
    ]
end


function evaluate_distances(chain, binding_pair_id, distance_constraints_by_binding_pair)
    distance_constraints = distance_constraints_by_binding_pair[binding_pair_id]

    [
        edge_point_linearized_signed_distance(
            chain,
            constraint
        ) for constraint in distance_constraints
    ]
end


function construct_constraint_jacobian(chain::RigidBodyChains.RigidBodyChain, distance_constraints_by_binding_pair)
    fixed_node = chain.fixed_rigid_body_node_id
    binding_pair_ids = collect(keys(distance_constraints_by_binding_pair))

    reduce(
        vcat,
        map(
            binding_pair_id -> reduce(vcat, construct_constraint_jacobian_rows(chain, binding_pair_id, distance_constraints_by_binding_pair)),
            binding_pair_ids
        )
    )
end


function get_initial_constraint_distances(chain, distance_constraints_by_binding_pair)
    binding_pair_ids = collect(keys(distance_constraints_by_binding_pair))

    collect(Iterators.flatten([
        evaluate_distances(chain, binding_pair_id, distance_constraints_by_binding_pair) for binding_pair_id in binding_pair_ids
   ]))
end


function construct_weight_vector(
    chain::RigidBodyChains.RigidBodyChain,
    weights_per_node::Array{Tuple{String, GeometryTypes.Point3f0}}
)

    weight_vector = zeros(get_number_of_variables(chain))

    for (node_name, weight) in weights_per_node
        node_id = RigidBodyChains.get_node_id_from_name(chain, node_name)
        variable_indices = get_configuration_variable_indices(chain, node_id)

        for (node_weight_index, weight_vector_index) in enumerate(variable_indices)
            weight_vector[weight_vector_index] = weight[node_weight_index]
        end
    end

    weight_vector
end


function get_chain_configuration_as_vector(chain::RigidBodyChains.RigidBodyChain)
    chain_configuration = zeros(get_number_of_variables(chain))

    for node in RigidBodyChains.get_all_node_ids(chain)
        node_configuration = RigidBodyChains.get_configuration(chain, node)
        configuration_variable_indices = get_configuration_variable_indices(chain, node)

        for (variable_value_index, variable_index) in enumerate(configuration_variable_indices)
            chain_configuration[variable_index] = node_configuration[variable_value_index]
        end
    end

    chain_configuration
end


function constrain_node_to_be_fixed(chain, node_id)
    node_x, node_y, node_theta = get_configuration_variable_indices(chain, node_id)

    x_vector_pos = zeros(1, get_number_of_variables(chain))
    x_vector_neg = zeros(1, get_number_of_variables(chain))
    y_vector_pos = zeros(1, get_number_of_variables(chain))
    y_vector_neg = zeros(1, get_number_of_variables(chain))
    theta_vector_pos = zeros(1, get_number_of_variables(chain))
    theta_vector_neg = zeros(1, get_number_of_variables(chain))
    distance_addon = zeros(6, 1)

    x_vector_pos[node_x] = 1
    x_vector_neg[node_x] = -1
    y_vector_pos[node_y] = 1
    y_vector_neg[node_y] = -1
    theta_vector_pos[node_theta] = 1
    theta_vector_neg[node_theta] = -1

    constraint_matrix_addition = reduce(vcat, [x_vector_pos, x_vector_neg, y_vector_pos, y_vector_neg, theta_vector_pos, theta_vector_neg])

    constraint_matrix_addition, distance_addon
end


function set_chain_to_configuration!(chain::RigidBodyChains.RigidBodyChain, configuration::Array)
    for index in 1:RigidBodyChains.get_number_of_rigid_bodies(chain)
        indices = LinearFlexibilityAnalysis.get_configuration_variable_indices(chain, index)

        if length(indices) > 0
            initial_cfg = RigidBodyChains.get_configuration(chain, index)
            node_configuration = GeometryTypes.Point3f0(
                configuration[indices]
            )

            RigidBodyChains.set_configuration!(chain, index, initial_cfg + node_configuration)
        end
    end
end


function solve_with_weights(constraint_jacobian::Array, initial_distances::Array, weights::Array)
    x = Convex.Variable(size(constraint_jacobian)[2])
    sparse_jacobian = SparseArrays.sparse(constraint_jacobian)

    opt_problem = Convex.maximize(Convex.dot(weights, x))
    opt_problem.constraints += sparse_jacobian * x + initial_distances >= 0
    
    solver = Clp.ClpSolver()
    if USE_GUROBI
        println("Using gurobi!")
        solver = Gurobi.GurobiSolver(OutputFlag=0)
    end

    Convex.solve!(opt_problem, solver)

    if opt_problem.status == :Infeasible || opt_problem.status == :Unbounded
        error("Optmization cannot continue. Problem is infeasible.")
    end

    x.value => opt_problem.optval
end


function find_nearest_feasible_configuration(constraint_jacobian, initial_distances)
    x = Convex.Variable(size(constraint_jacobian)[2])

    opt_problem = Convex.minimize(Convex.norm(x, 2))
    opt_problem.constraints += constraint_jacobian * x + initial_distances >= 0
    solver = SCS.SCSSolver(verbose=0, max_iters=50000)
    Convex.solve!(opt_problem, solver)

    x.value => opt_problem.optval
end


function get_maximum_error(chain, distance_constraints)
    max_error = 0.0

    constraint_distances = get_initial_constraint_distances(chain, distance_constraints)
    failed_constraints = constraint_distances[constraint_distances .< 0.0]

    if length(failed_constraints) > 0
        max_error = maximum(abs.(failed_constraints))
    end

    max_error
end


function get_objective_value(chain::RigidBodyChains.RigidBodyChain, weight_vector::Array)
    chain_configuration = get_chain_configuration_as_vector(chain)
    LinearAlgebra.dot(chain_configuration, weight_vector)
end


function find_maximum_time_step_with_sampling(chain, linear_program_solution, maximum_error, distance_constraints; timestep_spacing=0.01)
    """
    Compute the maximum time step in the given direction by sampling increasing time steps. If this function throws, it will
    leave the chain in an invalid state. Sorry!
    """

    best_timestep = 0.0

    for sample_timestep in 0.0:timestep_spacing:1.0
        delta_q = linear_program_solution * sample_timestep
        set_chain_to_configuration!(chain, delta_q)

        delta_q_error = get_maximum_error(chain, distance_constraints)

        if delta_q_error > maximum_error
            set_chain_to_configuration!(chain, -delta_q)
            return best_timestep
        end

        best_timestep = sample_timestep
        set_chain_to_configuration!(chain, -delta_q)
    end

    best_timestep
end


function get_separation_displacement(chain::RigidBodyChains.RigidBodyChain, separation_distance::Real)
    """
        get_separation_displacement(chain, separation_distance)

    Check if we can find a separation where the sum of all qdot value is greater than separation_distance
    but less than 2*separation_distance, if a separation is feasible then return a qdot => opt_value pair, otherwise
    nothing is returned.
    """
    weight_vector = [1.0 for _ in 1:get_number_of_variables(chain)]
    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)
    constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
    upper_bound_ones = fill(-1.0, 1, size(constraint_jacobian)[2])
    lower_bound_ones = fill(1.0, 1, size(constraint_jacobian)[2])
    for index = 1:size(constraint_jacobian)[2]
        if index % 3 == 0
                upper_bound_ones[1, index] = 0
                lower_bound_ones[1, index] = 0
        end
    end

    separation_jacobian = vcat(constraint_jacobian, upper_bound_ones, lower_bound_ones)

    # Start to solve separation
    separation_displacement = nothing
    try
        # negative direction
        separation_distances = vcat(initial_distances, [-separation_distance, 2*separation_distance])
        separation_displacement = solve_with_weights(separation_jacobian, separation_distances, weight_vector)
    catch
        # positive direction
        try
            separation_distances = vcat(initial_distances, [2*separation_distance, -separation_distance])
            separation_displacement = solve_with_weights(separation_jacobian, separation_distances, weight_vector)
        catch e
            @info "chain is not separable"
            separation_displacement = nothing
        end
    end

    return separation_displacement
end


function group_chain(chain; separation_distance=100)
    """
        group_chain(chian::RigidBodyChains.RigidBodyChain; separation_distance=100)

    Separate our chain to small group of chains. For node in chain if their are far enough to otherblocks we will believe they are separated from rest part.
    """
    node_label = Dict([(id, 0) for id in RigidBodyChains.get_all_node_ids(chain)])
    process_queue = [chain.fixed_rigid_body_node_id]
    current_label = 1

    while reduce(|, vcat(false, map(x -> x == 0, values(node_label))))
        while length(process_queue) > 0
            current_node_id = popfirst!(process_queue)
            node_label[current_node_id] = current_label

            neighbor_nodes = RigidBodyChains.neighbors(chain, current_node_id)
            for neighbor_node_id in neighbor_nodes
                if node_label[neighbor_node_id] == 0 && is_connected_blocks(chain, current_node_id, neighbor_node_id, separation_distance=separation_distance) && (! (neighbor_node_id in process_queue))
                    push!(process_queue, neighbor_node_id)
                end
            end
        end # End of while process_queue greater than 0
        for key in keys(node_label)
            if node_label[key] == 0
                push!(process_queue, key)
                break
            end
        end # End of for
        current_label += 1
    end

    # Based on flood-fill result, create new chains
    node_groups = Dict()
    for kv in node_label
        if kv.first == chain.fixed_rigid_body_node_id
            node_groups[kv.second] = pushfirst!(get(node_groups, kv.second, []), kv.first)
        else
            node_groups[kv.second] = push!(get(node_groups, kv.second, []), kv.first)
        end
    end
    # Return sub-chain groups
    [RigidBodyChains.sub_chain(chain, node_group_kv.second) node_group_kv in node_groups]
end


"""
    is_connected_blocks(chain, node_id_from::Integer, node_id_to::Integer; separation_distance::Real=100)

check if node_from and node_to are separated by checking the change of their configuration comparing to their initial configuraiton difference.
"""
function is_connected_blocks(chain, node_id_from::Integer, node_id_to::Integer; separation_distance=100)
    from_node_prop = RigidBodyChains.get_configuration(chain, node_id_from)
    to_node_prop = RigidBodyChains.get_configuration(chain, node_id_to)
    binding_pair = RigidBodyChains.get_binding_pair_from_src_dst_node(chain, node_id_from, node_id_to)
    new_relative_configuration = to_node_prop - from_node_prop
    relative_configuration_diff = new_relative_configuration - binding_pair.initial_relative_body_configuration
    # If change of configuration is less than 100, then blocks are not connected anymore.
    # Need a more precise way to determin this
    return LinearAlgebra.norm(relative_configuration_diff, 1) < separation_distance
end


function timestep_and_repair!(chain, distance_constraints, maximum_error, weight_vector; modifier=1, timestep_spacing=0.01, jacobian_addon=nothing, distance_addon=nothing, verbose=false)

    if verbose
        println("Constructing constraint jacobian, evaluating constraints...")
    end

    constraint_distances = get_initial_constraint_distances(chain, distance_constraints)
    constraint_jacobian = construct_constraint_jacobian(chain, distance_constraints)

    if verbose
        println("Finished!")
    end

    if jacobian_addon != nothing && distance_addon != nothing
        constraint_distances = vcat(constraint_distances, distance_addon)
        constraint_jacobian = vcat(constraint_jacobian, jacobian_addon)
    end

    movement_direction = LinearFlexibilityAnalysis.solve_with_weights(
        constraint_jacobian,
        constraint_distances,
        weight_vector
    ).first

    if verbose
        println("Finding time step...")
    end

    time_step = LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
        chain,
        movement_direction,
        maximum_error,
        distance_constraints,
        timestep_spacing=timestep_spacing
    )

    if verbose
        println("Found time step: ", time_step)
    end

    time_step *= modifier

    LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, movement_direction * time_step)

    time_step * movement_direction, time_step
end

function solve_and_resolve!(chain, distance_constraints, weight_vector)
    initial_cfg = get_chain_configuration_as_vector(chain)

    constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)
    constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)

    solution = solve_with_weights(constraint_jacobian, constraint_distances, weight_vector)
    set_chain_to_configuration!(chain, solution.first)

    constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)
    constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)

    solution = solve_with_weights(constraint_jacobian, constraint_distances, weight_vector)
    set_chain_to_configuration!(chain, solution.first)

    final_cfg = get_chain_configuration_as_vector(chain)

    final_cfg - initial_cfg
end

function construct_weight_vector_moving_all_bodies_toward_point(chain::RigidBodyChains.RigidBodyChain, point)
    weight_vector = zeros(get_number_of_variables(chain))

    for node_id in RigidBodyChains.get_all_node_ids(chain)
        node_configuration = RigidBodyChains.get_configuration(chain, node_id)
        
        configuration_indices = get_configuration_variable_indices(chain, node_id)

        if length(configuration_indices) > 0
            node_x, node_y, _ = get_configuration_variable_indices(chain, node_id)
            direction = point - node_configuration[1:2]

            weight_vector[node_x] = direction[1]
            weight_vector[node_y] = direction[2]
        end
    end

    weight_vector
end

function timestep_until_convergence!(
    chain,
    distance_constraints,
    maximum_error,
    weight_vector;
    modifier=1,
    timestep_spacing=0.01,
    min_gain=0.0,
    jacobian_addon=nothing,
    distance_addon=nothing,
    verbose=false
)
    # timestep until max displacement is less than the minimum
    number_iterations = 1
    previous_objective = 0.0
    current_gain = typemax(Float64)
    initial_configuration = get_chain_configuration_as_vector(chain)

    while current_gain > min_gain
        before_solve_configuration = get_chain_configuration_as_vector(chain)
        before_solve_objective = LinearAlgebra.dot(weight_vector, before_solve_configuration)

        current_displacement, time_step = timestep_and_repair!(
            chain,
            distance_constraints,
            maximum_error,
            weight_vector,
            modifier=modifier,
            timestep_spacing=timestep_spacing,
            jacobian_addon=jacobian_addon,
            distance_addon=distance_addon,
            verbose=verbose
        )

        after_solve_configuration = get_chain_configuration_as_vector(chain)
        after_solve_objective = LinearAlgebra.dot(weight_vector, after_solve_configuration)

        current_gain = after_solve_objective - before_solve_objective

        if verbose
            println("Timestepping iteration ", number_iterations, " gain ", current_gain)
        end

        number_iterations += 1
    end

    final_configuration = get_chain_configuration_as_vector(chain)

    number_iterations, final_configuration - initial_configuration
end


function construct_weight_vector_moving_all_bodies_toward_vertical_line(chain, line_x)
    weight_vector = zeros(get_number_of_variables(chain))

    for node in RigidBodyChains.get_all_node_ids(chain)
        node_variable_indices = get_configuration_variable_indices(chain, node)

        if length(node_variable_indices) > 0
            node_configuration = RigidBodyChains.get_configuration(chain, node)
            weight_vector[node_variable_indices[1]] = line_x - node_configuration[1]
        end
    end

    weight_vector
end

end
