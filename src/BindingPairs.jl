module BindingPairs

import LinearAlgebra

import GeometryTypes: Point3f0

import ..ConnectableRigidBodies: ConnectableRigidBodyID
import ..Geometry

const BindingPairID = String

"""
Represents data about how a pair of rigid bodies may connect. For each type of joint we work with
we should have two binding pairs that specify the relative position
"""
struct BindingPair
    from_node_id::Integer
    to_node_id::Integer
    binding_pair_id::BindingPairID
    base_body_type::ConnectableRigidBodyID
    relative_body_type::ConnectableRigidBodyID
    initial_relative_body_configuration::Point3f0
    valid_relative_body_transformations::Array{Point3f0, 1}
    is_sampled::Bool
end # struct BindingPair

function represent_same_joint_type(pair_a::BindingPair, pair_b::BindingPair)
    pair_a.base_body_type == pair_b.base_body_type &&
    pair_a.relative_body_type == pair_b.relative_body_type &&
    LinearAlgebra.norm(pair_a.initial_relative_body_configuration - pair_b.initial_relative_body_configuration) < 0.0001 
end

end # module BindingPairs
