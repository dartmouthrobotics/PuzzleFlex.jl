"""
Contains a model of a `RigidBodyChain` which is the highest level description of the of a system of lossely interlocked
rigid bodies. Rigid bodies are store stored in a connectivity graph where only the rigid bodies
connected by edges in this graph are assumed to interact with one another.

Several utility methods are documented which ease accessing and mutating properties of the underlying graph structure.
"""
module RigidBodyChains

import Base.Threads

import GeometryTypes: Point2f0, Point3f0
import LightGraphs: Graph, add_edge!, edges, src, dst, nv, vertices, reverse, Edge
import MetaGraphs: MetaGraph, set_props!, props, set_prop!
import JSON
import LightGraphs

import ..BindingPairs
import ..ConnectableRigidBodies
import ..Geometry


"""
Stores a system of rigid bodies where only the rigid bodies connected by edges
in `connectivity_graph` interfere with one another. Properties of the rigid bodies
are stored using vertex props. 

Each rigid body is a vertex in the graph. `LightGraphs` assigns
an id to each vertex added into the graph. In this code, that id is referenced as
`node_id`. Edges are described with `BindingPair` objects.
Each edge has a forward and a backward binding pair specifying two directions of interaction
between the vertex.
"""
mutable struct RigidBodyChain
    connectable_rigid_bodies::Dict{
        ConnectableRigidBodies.ConnectableRigidBodyID,
        ConnectableRigidBodies.ConnectableRigidBody
    }
    connectivity_graph::MetaGraph
    fixed_rigid_body_name
    fixed_rigid_body_node_id
end # struct


"""
Parses a rigid body chain from a json file
"""
RigidBodyChain(parsed_json::Dict) = load_from_json(parsed_json)
Base.copy(chain::RigidBodyChain) = begin
    RigidBodyChain(
        copy(chain.connectable_rigid_bodies),
        copy(chain.connectivity_graph),
        chain.fixed_rigid_body_name,
        chain.fixed_rigid_body_node_id
    )
end

const CONFIGURATION_PROP = :rigid_body_configuration
const NODE_NAME_PROP = :node_name
const RIGID_BODY_TYPE_PROP = :connectable_rigid_body_type
const FORWARD_BINDING_PAIR_PROP = :forward_binding_pair
const BACKWARD_BINDING_PAIR_PROP = :backward_binding_pair

"""
    get_chain_with_offset_boundaries(base_chain, offset_amount)

Returns a new chain with the same connectivity as `base_chain` with each rigid body's
boundary offset by `offset_amount`. Positive offset amounts grow the boundary and negative ones
shrink it
"""
function get_chain_with_offset_boundaries(base_chain, offset_amount)
    new_chain = copy(base_chain)

    for rigid_body_id in keys(new_chain.connectable_rigid_bodies)
        original_body = new_chain.connectable_rigid_bodies[rigid_body_id].rigid_body

        new_chain.connectable_rigid_bodies[rigid_body_id] = ConnectableRigidBodies.ConnectableRigidBody(
            rigid_body_id,
            RigidBodies.get_rigid_body_with_offset_boundary(original_body, offset_amount)
        )
    end

    new_chain
end


"""
    get_node_name(chain::RigidBodyChain, node_id)

Returns the human readable name for a the rigid body with id `node_id`.
"""
function get_node_name(chain::RigidBodyChain, node_id)
    props(chain.connectivity_graph, node_id)[NODE_NAME_PROP]
end


"""
    get_node_id_from_name(chain::RigidBodyChain, node_name)

Returns the internal id for the rigid body with name `node_name` in the given `chain`
"""
function get_node_id_from_name(chain::RigidBodyChain, node_name)
    graph_vertices = vertices(chain.connectivity_graph)
    nodes_with_matching_name = [
        node for node in graph_vertices if props(chain.connectivity_graph, node)[NODE_NAME_PROP] == node_name
    ]

    if length(nodes_with_matching_name) == 0
        return nothing
    end

    nodes_with_matching_name[1]
end


"""
    get_all_node_ids(chain::RigidBodyChain)

Returns all rigid body ids making up the chain
"""
function get_all_node_ids(chain::RigidBodyChain)
    vertices(chain.connectivity_graph)
end


"""
    get_all_unfixed_node_ids(chain::RigidBodyChain)

Returns all rigid body ids except for the rigid body designated as the fixed rigid body.
"""
function get_all_unfixed_node_ids(chain::RigidBodyChain)
    filter(
        node_id -> node_id != chain.fixed_rigid_body_node_id,
        get_all_node_ids(chain)
    )
end


"""
    get_binding_pair_from_name(chain::RigidBodyChain, binding_pair_name)

Gets the `BingindPair` with name `binding_pair_name`
"""
function get_binding_pair_from_name(chain::RigidBodyChain, binding_pair_name)
    edges_with_matching_binding_pair = [
        edge for edge in edges(chain.connectivity_graph) if
            props(chain.connectivity_graph, edge)[FORWARD_BINDING_PAIR_PROP].binding_pair_id == binding_pair_name ||
            props(chain.connectivity_graph, edge)[BACKWARD_BINDING_PAIR_PROP].binding_pair_id == binding_pair_name
    ]

    if length(edges_with_matching_binding_pair) == 0
        return nothing
    end

    edge = edges_with_matching_binding_pair[1]

    forward, backward = (
        props(chain.connectivity_graph, edge)[FORWARD_BINDING_PAIR_PROP],
        props(chain.connectivity_graph, edge)[BACKWARD_BINDING_PAIR_PROP]
    )

    if forward.binding_pair_id == binding_pair_name
        return forward
    end

    return backward
end


function load_rigid_body_types_from_json(rigid_body_types_files)
    constructed_body_types = map(
        ConnectableRigidBodies.load_from_json,
        map(
            file_path -> JSON.parse(read(open(file_path["path"]), String)),
            rigid_body_types_files
        )
    )

    Dict(
        body_type.connectable_body_id => body_type for body_type in constructed_body_types
    )
end


"""
    neighbors(chain::RigidBodyChain, node_id)

Returns the rigid bodies connected to the body with id `node_id`
"""
function neighbors(chain::RigidBodyChain, node_id)
    LightGraphs.outneighbors(chain.connectivity_graph, node_id)
end


"""
    get_number_of_rigid_bodies(chain::RigidBodyChain)

Returns the number of rigid bodies making up the given chain
"""
function get_number_of_rigid_bodies(chain::RigidBodyChain)
    nv(chain.connectivity_graph)
end


function load_connectivity_graph_from_json(graph_json)
    number_nodes = length(keys(graph_json))
    graph = Graph(number_nodes)

    node_name_to_node_number = Dict(
        key => index for (index, key) in enumerate(keys(graph_json))
    )

    for (node_name, current_node) in node_name_to_node_number
        out_edges = [node_name_to_node_number[edge_dict["target"]] for edge_dict in graph_json[node_name]["edges"]]

        for edge_target in out_edges
            add_edge!(graph, current_node, edge_target)
        end
    end

    connectivity_graph = MetaGraph(graph)

    for (node_name, current_node) in node_name_to_node_number
        initial_configuration = Point3f0(
            graph_json[node_name]["configuration"]["x"],
            graph_json[node_name]["configuration"]["y"],
            graph_json[node_name]["configuration"]["theta"]
        )

        set_props!(
            connectivity_graph,
            current_node,
            Dict(
                RIGID_BODY_TYPE_PROP => graph_json[node_name]["body_name"],
                NODE_NAME_PROP => node_name,
                CONFIGURATION_PROP => initial_configuration,
            )
        )
    end

    connectivity_graph
end


function construct_binding_pair(connectivity_graph, edge)
    source_props = props(connectivity_graph, src(edge))
    dest_props = props(connectivity_graph, dst(edge))

    source_name = source_props[NODE_NAME_PROP]
    dest_name = dest_props[NODE_NAME_PROP]

    source_body_type = source_props[RIGID_BODY_TYPE_PROP]
    dest_body_type = dest_props[RIGID_BODY_TYPE_PROP]

    source_configuration = source_props[CONFIGURATION_PROP]
    dest_configuration = dest_props[CONFIGURATION_PROP]

    binding_pair_id = source_name * " => " * dest_name

    initial_relative_body_configuration = dest_configuration - source_configuration

    BindingPairs.BindingPair(
        src(edge),
        dst(edge),
        binding_pair_id,
        source_body_type,
        dest_body_type,
        initial_relative_body_configuration,
        [],
        false
    )
end


function add_binding_pairs_to_edges!(connectivity_graph)
    for edge in edges(connectivity_graph)
        forward_binding_pair = construct_binding_pair(connectivity_graph, edge)
        backward_binding_pair = construct_binding_pair(connectivity_graph, reverse(edge))

        set_props!(
            connectivity_graph,
            edge,
            Dict(
                FORWARD_BINDING_PAIR_PROP => forward_binding_pair,
                BACKWARD_BINDING_PAIR_PROP => backward_binding_pair
            )
        )

    end
end


function add_binding_pair_to_edge_props!(chain, binding_pair, edge)
    if is_reverse_edge(chain, Edge(binding_pair.from_node_id, binding_pair.to_node_id))
        set_prop!(chain.connectivity_graph, edge, BACKWARD_BINDING_PAIR_PROP, binding_pair)
    else
        set_prop!(chain.connectivity_graph, edge, FORWARD_BINDING_PAIR_PROP, binding_pair)
    end
end


"""
    get_node_type(chain::RigidBodyChains.RigidBodyChain, node_id::Integer)

get current node_id corresponding node's type
"""
function get_node_type(chain::RigidBodyChain, node_id)
    props(chain.connectivity_graph, node_id)[RIGID_BODY_TYPE_PROP]
end


"""
    get_configuration(chain::RigidBodyChain, node_id)

Returns the configuration used to map the given rigid body
into world coordinates. The x, y parameters are the displacement of the local coordinate frame from the world origin, and theta is the rotation of the rigid body about the origin of its local frame.
"""
function get_configuration(chain::RigidBodyChain, node_id)
    props(chain.connectivity_graph, node_id)[CONFIGURATION_PROP]
end


"""
    set_configuration!(chain::RigidBodyChain, node_id, configuration::Point3f0)

Sets the configuration of the given rigid body.
"""
function set_configuration!(chain::RigidBodyChain, node_id, configuration::Point3f0)
    set_prop!(chain.connectivity_graph, node_id, CONFIGURATION_PROP, configuration)
end


function is_reverse_edge(chain::RigidBodyChain, edge_id)
    !any([edge == edge_id for edge in edges(chain.connectivity_graph)]) &&
    any([edge == reverse(edge_id) for edge in edges(chain.connectivity_graph)])
end


"""
    get_binding_pair(chain::RigidBodyChain, edge_id)

Returns the binding pair associated with the given `edge_id` where `edge_id` is the id of the edge in the `LightGraphs` graph.
"""
function get_binding_pair(chain::RigidBodyChain, edge_id)
    if is_reverse_edge(chain, edge_id)
        return props(chain.connectivity_graph, edge_id)[BACKWARD_BINDING_PAIR_PROP]
    end
    props(chain.connectivity_graph, edge_id)[FORWARD_BINDING_PAIR_PROP]
end


function get_binding_pair_from_src_dst_node(chain::RigidBodyChain, node_id_from, node_id_to)
    """
    Return binding_pair from vertex node_id_from to vertex node_id_to
    """
    # Create all list of all edges
    graph_edges = []
    for edge in edges(chain.connectivity_graph)
        push!(graph_edges, edge)
        push!(graph_edges, reverse(edge))
    end
    # Get the edge we want to have
    result = filter(x -> src(x)==node_id_from && dst(x)==node_id_to, graph_edges)
    if length(result) > 1
        @warn "Found more than one edge from "*string(node_id_from)*" to "*string(node_id_to)
    elseif length(result) < 1
        @error "Cannot find edge from "*string(node_id_from)*" to "*string(node_id_to)
    end
    get_binding_pair(chain, result[1])
end


function get_all_forward_binding_pairs(chain::RigidBodyChain)
    [
        props(chain.connectivity_graph, edge)[FORWARD_BINDING_PAIR_PROP] for edge in edges(chain.connectivity_graph)
    ]
end


"""
    get_all_backward_binding_pairs(chain::RigidBodyChain)

Returns all binding pairs which are oriented in a different direction than their underlying LightGraphs edge in the connectivity graph of the chain
"""
function get_all_backward_binding_pairs(chain::RigidBodyChain)
    [
        props(chain.connectivity_graph, edge)[BACKWARD_BINDING_PAIR_PROP] for edge in edges(chain.connectivity_graph)
    ]
end


"""
get_all_binding_pairs(chain::RigidBodyChain)

Returns all binding pairs which are oriented in the same direction as their underlying LightGraphs edge in the connectivity graph of the chain
"""
function get_all_binding_pairs(chain::RigidBodyChain)
    vcat(
        get_all_forward_binding_pairs(chain),
        get_all_backward_binding_pairs(chain)
    )
end


"""
    get_rigid_body_boundary(chain::RigidBodyChain, node_id; apply_configuration_map=true)

Returns the boundary of the rigid body with id `node_id`. If `apply_configuration_map` is `true`, the vertices of the rigid body are mapped into the world frame. Otherwise, the boundary is not mapped.
"""
function get_rigid_body_boundary(chain::RigidBodyChain, node_id; apply_configuration_map=true)
    node_props = props(chain.connectivity_graph, node_id)
    configuration = node_props[CONFIGURATION_PROP]
    transform = Geometry.construct_frame_transform(configuration)

    untransformed_boundary = chain.connectable_rigid_bodies[node_props[RIGID_BODY_TYPE_PROP]].rigid_body.boundary

    if apply_configuration_map
        return map(Point2f0, map(transform, untransformed_boundary))
    end

    untransformed_boundary
end


function get_top_right_node_id(chain::RigidBodyChain)
    sort(vertices(chain.connectivity_graph), by=vertex -> props(chain.connectivity_graph, vertex)[CONFIGURATION_PROP], rev=true)[1]
end



function get_bottom_right_node_id(chain::RigidBodyChain)
    sort(vertices(chain.connectivity_graph), by=vertex -> begin
            location = props(chain.connectivity_graph, vertex)[CONFIGURATION_PROP]
            [
                location[2],
                1.0 / location[1]
            ]
        end,
        rev=false
   )[1]
end


"""
    get_top_right_node_name(chain::RigidBodyChain)

Returns the human readable name of the node with the top right local coordinate frame origin.
"""
function get_top_right_node_name(chain::RigidBodyChain)
    top_right_id = get_top_right_node_id(chain)
    get_node_name(chain, top_right_id)
end


"""
    get_bottom_right_node_name(chain::RigidBodyChain)

Returns the human readable name of the node with the bottom rightmost local coordinate frame origin.
"""
function get_bottom_right_node_name(chain::RigidBodyChain)
    bottom_right_id = get_bottom_right_node_id(chain)
    get_node_name(chain, bottom_right_id)
end


"""
    load_from_json(parsed_json::Dict)

Loads the rigid body chain from a properly formatted json file.
"""
function load_from_json(parsed_json::Dict)
    connectable_rigid_bodies = load_rigid_body_types_from_json(
        parsed_json["RigidBodyChain"]["RigidBodyTypes"]
    )

    connectivity_graph = load_connectivity_graph_from_json(
        parsed_json["RigidBodyChain"]["ConnectivityGraph"]
    )

    fixed_rigid_body_name = parsed_json["RigidBodyChain"]["FixedRigidBodyNode"]
    fixed_rigid_body_node_id = [
        node_id for node_id in vertices(connectivity_graph) if props(connectivity_graph, node_id)[NODE_NAME_PROP] == fixed_rigid_body_name
   ][1]

    add_binding_pairs_to_edges!(connectivity_graph)

    RigidBodyChain(
        connectable_rigid_bodies,
        connectivity_graph,
        fixed_rigid_body_name,
        fixed_rigid_body_node_id,
    )
end


"""
    construct_sub_chain_graph_from_chain(chain::RigidBodyChain, sub_chain_node_ids::Array{Integer})

Construct connectivity_graph for sub chain. All connection between nodes sub_chain_node_ids in original chain will be kept in sub chain.
But other connections will be ignored.
"""
function construct_sub_chain_graph_from_chain(chain::RigidBodyChain, sub_chain_node_ids)
    number_nodes = length(sub_chain_node_ids)
    graph = Graph(number_nodes)

    old_node_name_to_node_number = Dict(props(chain.connectivity_graph, node_id)[NODE_NAME_PROP] => node_id for node_id in sub_chain_node_ids)
    node_name_to_node_number = Dict(props(chain.connectivity_graph, key)[NODE_NAME_PROP] => index for (index, key) in enumerate(sub_chain_node_ids))

    for (node_name, current_node) in node_name_to_node_number
        # find current_node old neighbors, for these who are still in sub_chain, map their node name to new node_id
        current_node_neighbors_new_ids = [node_name_to_node_number[props(chain.connectivity_graph, node_id)[NODE_NAME_PROP]] for node_id in neighbors(chain, get_node_id_from_name(chain, node_name)) if node_id in sub_chain_node_ids]
        # In new graph, build edges
        for edge_target in current_node_neighbors_new_ids
            add_edge!(graph, current_node, edge_target)
        end
    end

    connectivity_graph = MetaGraph(graph)

    for (node_name, current_node) in node_name_to_node_number
        configuration_in_old_chain = get_configuration(chain, old_node_name_to_node_number[node_name])
        initial_configuration = Point3f0(configuration_in_old_chain[1],
                                         configuration_in_old_chain[2],
                                         configuration_in_old_chain[3])
        set_props!(
            connectivity_graph,
            current_node,
            Dict(
                RIGID_BODY_TYPE_PROP => props(chain.connectivity_graph, old_node_name_to_node_number[node_name])[RIGID_BODY_TYPE_PROP],
                NODE_NAME_PROP => node_name,
                CONFIGURATION_PROP => initial_configuration,
                UPPER_BOUNDING_CONFIGURATION_SET_PROP => nothing,
            )
        )
    end
    connectivity_graph
end


"""
    output_as_text_file(chain::RigidBodyChain, output_path)

Outputs the chain to a text file containing the boundary of each rigid body as a polygon
"""
function output_as_text_file(chain::RigidBodyChain, output_path)
    textfile_lines = []

    top_right_node_id = get_top_right_node_id(chain)
    fixed_node_id = chain.fixed_rigid_body_node_id
    node_ids = get_all_node_ids(chain)

    for node_id in node_ids
        node_boundary = get_rigid_body_boundary(chain, node_id)
        push!(textfile_lines, "$(length(node_boundary) - 1)")

        for vertex in node_boundary[1:end-1]
            push!(textfile_lines, "$(vertex[1]) $(vertex[2])")
        end

        push!(textfile_lines, "")
    end

    top_right_idx = findfirst(isequal(top_right_node_id), node_ids)
    fixed_idx = findfirst(isequal(fixed_node_id), node_ids)

    push!(textfile_lines, fixed_idx)
    push!(textfile_lines, top_right_idx)

    output_string = join(textfile_lines, "\n")

    output_file = open(output_path, "w")
    write(output_file, output_string)
    close(output_file)
end

"""
    sub_chain(chain::RigidBodyChains.RigidBodyChain, sub_chain_node_ids::Array{Integer})

Contruct a sub chain from current chain. All rigid_bodies will be kept. And for connectivity_graph, all connection between nodes sub_chain_node_ids in original chain will be kept in sub chain.
But other connections will be ignored. The first element in sub_chain_node_ids will be used as fixed rigid body.
"""
function sub_chain(chain::RigidBodyChain, sub_chain_node_ids)
    if length(sub_chain_node_ids) < 1
        @error "node ids given to sub_chain is less than 1"
    end
    connectable_rigid_bodies = deepcopy(chain.connectable_rigid_bodies)
    connectivity_graph = construct_sub_chain_graph_from_chain(chain, sub_chain_node_ids)
    # By default, use first node in sub_chain_node_ids as fixed_node
    fixed_rigid_body_name = props(chain.connectivity_graph, sub_chain_node_ids[1])[NODE_NAME_PROP]
    # Get new node_id in new graph
    fixed_rigid_body_node_id = [node_id for node_id in vertices(connectivity_graph) if props(connectivity_graph, node_id)[NODE_NAME_PROP] == fixed_rigid_body_name][1]

    add_binding_pairs_to_edges!(connectivity_graph)

    RigidBodyChain(
        connectable_rigid_bodies,
        connectivity_graph,
        fixed_rigid_body_name,
        fixed_rigid_body_node_id
    )
end


end # module RigidBodyChains
