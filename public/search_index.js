var documenterSearchIndex = {"docs": [

{
    "location": "#",
    "page": "Home",
    "title": "Home",
    "category": "page",
    "text": ""
},

{
    "location": "#Home-1",
    "page": "Home",
    "title": "Home",
    "category": "section",
    "text": "(Image: header)PuzzleFlex.jl provides a way to create a graph representation of an interconnected chain of rigid bodies through the RigidBodyChain struct from the RigidBodyChains module. The LinearFlexibilityAnalysis module provides a way of translating a RigidBodyChain to a linear program which can be utilized to find extreme configurations of large chains in a variety of directions in configuration space. The linear program finds a displacement D in the configuration space of the chain which maximizes the product DC where C is a desired configuration in configuration space.  For example, the following code moves the top right rigid body of the nine link grid specified from a JSON file as far as possible to the right.using PuzzleFlex\nimport GeometryTypes\n\n# sets the maximum error used for selecting time steps\n# between solutions of the linear program\nmaximum_error = 0.0\n\nchain = Utils.load_chain_from_file(\"json-input-chains/nine-link-grid.json\")\n\n# distance constraints are linear distance constraints between edges\n# and points of connected rigid bodies\ndistance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(\n    chain\n)\n\n# each node is given a human readable name\ntop_right_node_name = RigidBodyChains.get_top_right_node_name(chain)\n\n# construct a cost function for the linear program which moves the top right\n# node of the chain as much as possible to the right\nobjective_vector = LinearFlexibilityAnalysis.construct_weight_vector(\n    chain,\n    [\n        (top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0))\n    ]\n)\n\n# repeatedly solves the linear program constructed from\n# distance_constraints and objective_vector\n# while limiting the error introduced to below maximum_error\nLinearFlexibilityAnalysis.timestep_until_convergence!(\n    chain,\n    distance_constraints,\n    maximum_error,\n    objective_vector\n)"
},

{
    "location": "#Specifying-a-Chain-1",
    "page": "Home",
    "title": "Specifying a Chain",
    "category": "section",
    "text": "Currently, a rigid body chain can be specified with either a Dict (parsed from a json file) or a PNG image."
},

{
    "location": "#JSON-1",
    "page": "Home",
    "title": "JSON",
    "category": "section",
    "text": "The JSON format for specifying a chain allows more flexibility over the PNG image, but requires more manual intervention. Several examples of the format can be found in the json-input-chains directory. An example of the format is shown below. The purpose of each key is explained below.{\n    \"RigidBodyChain\": {\n        \"RigidBodyTypes\": [\n            {\n                \"path\": \"json-input-chains/json-chain-links/20mm-female-four-sided-square.json\"\n            },\n            {\n                \"path\": \"json-input-chains/json-chain-links/20mm-male-four-sided-square.json\"\n            }\n        ],\n        \"ConnectivityGraph\": {\n            \"node1\": {\n                \"body_name\": \"20mm-female-four-sided-square\",\n                \"edges\": [\n                    {\"target\": \"node2\"}\n                ],\n                \"configuration\": {\n                    \"x\": 10.0,\n                    \"y\": 0.0,\n                    \"theta\": 0.0\n                }\n            },\n            \"node2\": {\n                \"body_name\": \"20mm-male-four-sided-square\",\n                \"edges\": [\n                    {\"target\": \"node1\"}\n                ],\n                \"configuration\": {\n                    \"x\": 30.5,\n                    \"y\": 0.0,\n                    \"theta\": 0.0\n                }\n\n            }\n        },\n        \"FixedRigidBodyNode\": \"node1\"\n    }\n}\"RigidBodyTypes\" is the list of each rigid body type. Each rigid body type specifies a boundary of 2D points and is given a name within its json file. path is the disk location of the json file specifying the rigid body type. Boundary coordinates are specified in a local frame where the origin should be roughly in the middle of the boundary.\n\"ConnectivityGraph\" dictates which rigid bodies may interact with one another. The connectivity graph is a dictionary of vertices in the graph keyed by the human readable name of each vertex. Each vertex contains:\n\"body_name\" The name of the rigid body type whose boundary is used for this vertex\n\"edges\" A list of edges from the current vertex ending in vertices specified by a human readable name\n\"configuration\" The configuration of this rigid body in world coordinates. theta is the angle in radians that the boundary of the rigid body has been rotated about its local frame origin.\n\"FixedRigidBodyNode\" The name of the vertex in the chain\'s graph which is assumed to be fixed. If no node should be fixed, you can make a \"ghost\" node and fix that. See the flock control example for more on this."
},

{
    "location": "#PNG-1",
    "page": "Home",
    "title": "PNG",
    "category": "section",
    "text": "You have the option of specifying a chain as a PNG image where all pixels which are black are used to create a chain with only two types of rigid bodies which are the same shape as a pixel (square)."
},

{
    "location": "reference/#",
    "page": "Reference",
    "title": "Reference",
    "category": "page",
    "text": ""
},

{
    "location": "reference/#Reference-1",
    "page": "Reference",
    "title": "Reference",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains",
    "category": "module",
    "text": "Contains a model of a RigidBodyChain which is the highest level description of the of a system of lossely interlocked rigid bodies. Rigid bodies are store stored in a connectivity graph where only the rigid bodies connected by edges in this graph are assumed to interact with one another.\n\nSeveral utility methods are documented which ease accessing and mutating properties of the underlying graph structure.\n\n\n\n\n\n"
},

{
    "location": "reference/#Module-RigidBodyChains-1",
    "page": "Reference",
    "title": "Module RigidBodyChains",
    "category": "section",
    "text": "CurrentModule = PuzzleFlexRigidBodyChains"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.RigidBodyChain",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.RigidBodyChain",
    "category": "type",
    "text": "Stores a system of rigid bodies where only the rigid bodies connected by edges in connectivity_graph interfere with one another. Properties of the rigid bodies are stored using vertex props. \n\nEach rigid body is a vertex in the graph. LightGraphs assigns an id to each vertex added into the graph. In this code, that id is referenced as node_id. Edges are described with BindingPair objects. Each edge has a forward and a backward binding pair specifying two directions of interaction between the vertex.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_node_name",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_node_name",
    "category": "function",
    "text": "get_node_name(chain::RigidBodyChain, node_id)\n\nReturns the human readable name for a the rigid body with id node_id.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_node_id_from_name",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_node_id_from_name",
    "category": "function",
    "text": "get_node_id_from_name(chain::RigidBodyChain, node_name)\n\nReturns the internal id for the rigid body with name node_name in the given chain\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_all_node_ids",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_all_node_ids",
    "category": "function",
    "text": "get_all_node_ids(chain::RigidBodyChain)\n\nReturns all rigid body ids making up the chain\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_chain_with_offset_boundaries",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_chain_with_offset_boundaries",
    "category": "function",
    "text": "get_chain_with_offset_boundaries(base_chain, offset_amount)\n\nReturns a new chain with the same connectivity as base_chain with each rigid body\'s boundary offset by offset_amount. Positive offset amounts grow the boundary and negative ones shrink it\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_all_unfixed_node_ids",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_all_unfixed_node_ids",
    "category": "function",
    "text": "get_all_unfixed_node_ids(chain::RigidBodyChain)\n\nReturns all rigid body ids except for the rigid body designated as the fixed rigid body.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_binding_pair_from_name",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_binding_pair_from_name",
    "category": "function",
    "text": "get_binding_pair_from_name(chain::RigidBodyChain, binding_pair_name)\n\nGets the BingindPair with name binding_pair_name\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.neighbors",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.neighbors",
    "category": "function",
    "text": "neighbors(chain::RigidBodyChain, node_id)\n\nReturns the rigid bodies connected to the body with id node_id\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_number_of_rigid_bodies",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_number_of_rigid_bodies",
    "category": "function",
    "text": "get_number_of_rigid_bodies(chain::RigidBodyChain)\n\nReturns the number of rigid bodies making up the given chain\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_configuration",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_configuration",
    "category": "function",
    "text": "get_configuration(chain::RigidBodyChain, node_id)\n\nReturns the configuration used to map the given rigid body into world coordinates. The x, y parameters are the displacement of the local coordinate frame from the world origin, and theta is the rotation of the rigid body about the origin of its local frame.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.output_as_text_file",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.output_as_text_file",
    "category": "function",
    "text": "output_as_text_file(chain::RigidBodyChain, output_path)\n\nOutputs the chain to a text file containing the boundary of each rigid body as a polygon\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.set_configuration!",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.set_configuration!",
    "category": "function",
    "text": "set_configuration!(chain::RigidBodyChain, node_id, configuration::Point3f0)\n\nSets the configuration of the given rigid body.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_binding_pair",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_binding_pair",
    "category": "function",
    "text": "get_binding_pair(chain::RigidBodyChain, edge_id)\n\nReturns the binding pair associated with the given edge_id where edge_id is the id of the edge in the LightGraphs graph.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_rigid_body_boundary",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_rigid_body_boundary",
    "category": "function",
    "text": "get_rigid_body_boundary(chain::RigidBodyChain, node_id; apply_configuration_map=true)\n\nReturns the boundary of the rigid body with id node_id. If apply_configuration_map is true, the vertices of the rigid body are mapped into the world frame. Otherwise, the boundary is not mapped.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_all_binding_pairs",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_all_binding_pairs",
    "category": "function",
    "text": "getallbinding_pairs(chain::RigidBodyChain)\n\nReturns all binding pairs which are oriented in the same direction as their underlying LightGraphs edge in the connectivity graph of the chain\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.load_from_json",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.load_from_json",
    "category": "function",
    "text": "load_from_json(parsed_json::Dict)\n\nLoads the rigid body chain from a properly formatted json file.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_bottom_right_node_name",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_bottom_right_node_name",
    "category": "function",
    "text": "get_bottom_right_node_name(chain::RigidBodyChain)\n\nReturns the human readable name of the node with the bottom rightmost local coordinate frame origin.\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_all_backward_binding_pairs",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_all_backward_binding_pairs",
    "category": "function",
    "text": "get_all_backward_binding_pairs(chain::RigidBodyChain)\n\nReturns all binding pairs which are oriented in a different direction than their underlying LightGraphs edge in the connectivity graph of the chain\n\n\n\n\n\n"
},

{
    "location": "reference/#PuzzleFlex.RigidBodyChains.get_top_right_node_name",
    "page": "Reference",
    "title": "PuzzleFlex.RigidBodyChains.get_top_right_node_name",
    "category": "function",
    "text": "get_top_right_node_name(chain::RigidBodyChain)\n\nReturns the human readable name of the node with the top right local coordinate frame origin.\n\n\n\n\n\n"
},

{
    "location": "reference/#Contents-1",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": "CurrentModule = PuzzleFlex.RigidBodyChainsRigidBodyChainget_node_nameget_node_id_from_nameget_all_node_idsget_chain_with_offset_boundariesget_all_unfixed_node_idsget_binding_pair_from_nameneighborsget_number_of_rigid_bodiesget_configurationoutput_as_text_file set_configuration!get_binding_pairget_rigid_body_boundaryget_all_binding_pairsload_from_jsonget_bottom_right_node_nameget_all_backward_binding_pairsget_top_right_node_name"
},

{
    "location": "reference/#Module-LinearFlexibilityAnalysis-1",
    "page": "Reference",
    "title": "Module LinearFlexibilityAnalysis",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-2",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-BindingPairs-1",
    "page": "Reference",
    "title": "Module BindingPairs",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-3",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-Geometry-1",
    "page": "Reference",
    "title": "Module Geometry",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-4",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-RigidBodies-1",
    "page": "Reference",
    "title": "Module RigidBodies",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-5",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-Visualization-1",
    "page": "Reference",
    "title": "Module Visualization",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-6",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-PNGChainLoader-1",
    "page": "Reference",
    "title": "Module PNGChainLoader",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-7",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-Visualization-2",
    "page": "Reference",
    "title": "Module Visualization",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-8",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-RigidBodies-2",
    "page": "Reference",
    "title": "Module RigidBodies",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-9",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Module-Utils-1",
    "page": "Reference",
    "title": "Module Utils",
    "category": "section",
    "text": ""
},

{
    "location": "reference/#Contents-10",
    "page": "Reference",
    "title": "Contents",
    "category": "section",
    "text": ""
},

]}
