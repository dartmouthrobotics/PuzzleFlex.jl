<!DOCTYPE html>
<html lang="en"><head><meta charset="UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"/><title>Home · PuzzleFlex.jl</title><link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css" rel="stylesheet" type="text/css"/><link href="https://fonts.googleapis.com/css?family=Lato|Roboto+Mono" rel="stylesheet" type="text/css"/><link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"/><link href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css" rel="stylesheet" type="text/css"/><script>documenterBaseURL="."</script><script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.2.0/require.min.js" data-main="assets/documenter.js"></script><script src="siteinfo.js"></script><script src="../versions.js"></script><link href="assets/documenter.css" rel="stylesheet" type="text/css"/></head><body><nav class="toc"><h1>PuzzleFlex.jl</h1><select id="version-selector" onChange="window.location.href=this.value" style="visibility: hidden"></select><form class="search" id="search-form" action="search/"><input id="search-query" name="q" type="text" placeholder="Search docs"/></form><ul><li class="current"><a class="toctext" href>Home</a><ul class="internal"><li class="toplevel"><a class="toctext" href="#Specifying-a-Chain-1">Specifying a Chain</a></li><li><a class="toctext" href="#JSON-1">JSON</a></li><li><a class="toctext" href="#PNG-1">PNG</a></li></ul></li><li><a class="toctext" href="reference/">Reference</a></li></ul></nav><article id="docs"><header><nav><ul><li><a href>Home</a></li></ul><a class="edit-page" href="https://gitlab.com/dartmouthrobotics/PuzzleFlex.jl"><span class="fa"></span> Edit on GitLab</a></nav><hr/><div id="topbar"><span>Home</span><a class="fa fa-bars" href="#"></a></div></header><h1><a class="nav-anchor" id="Home-1" href="#Home-1">Home</a></h1><p><img src="assets/constrained-chain.png" alt="header"/></p><p>PuzzleFlex.jl provides a way to create a graph representation of an interconnected chain of rigid bodies through the <code>RigidBodyChain</code> struct from the <code>RigidBodyChains</code> module. The <code>LinearFlexibilityAnalysis</code> module provides a way of translating a <code>RigidBodyChain</code> to a linear program which can be utilized to find extreme configurations of large chains in a variety of directions in configuration space. The linear program finds a displacement <code>D</code> in the configuration space of the chain which maximizes the product <code>DC</code> where <code>C</code> is a desired configuration in configuration space.  </p><p>For example, the following code moves the top right rigid body of the nine link grid specified from a JSON file as far as possible to the right.</p><pre><code class="language-julia">using PuzzleFlex
import GeometryTypes

# sets the maximum error used for selecting time steps
# between solutions of the linear program
maximum_error = 0.0

chain = Utils.load_chain_from_file(&quot;json-input-chains/nine-link-grid.json&quot;)

# distance constraints are linear distance constraints between edges
# and points of connected rigid bodies
distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(
    chain
)

# each node is given a human readable name
top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)

# construct a cost function for the linear program which moves the top right
# node of the chain as much as possible to the right
objective_vector = LinearFlexibilityAnalysis.construct_weight_vector(
    chain,
    [
        (top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0))
    ]
)

# repeatedly solves the linear program constructed from
# distance_constraints and objective_vector
# while limiting the error introduced to below maximum_error
LinearFlexibilityAnalysis.timestep_until_convergence!(
    chain,
    distance_constraints,
    maximum_error,
    objective_vector
)</code></pre><h1><a class="nav-anchor" id="Specifying-a-Chain-1" href="#Specifying-a-Chain-1">Specifying a Chain</a></h1><p>Currently, a rigid body chain can be specified with either a Dict (parsed from a json file) or a PNG image.</p><h2><a class="nav-anchor" id="JSON-1" href="#JSON-1">JSON</a></h2><p>The JSON format for specifying a chain allows more flexibility over the PNG image, but requires more manual intervention. Several examples of the format can be found in the <code>json-input-chains</code> directory. An example of the format is shown below. The purpose of each key is explained below.</p><pre><code class="language-json">{
    &quot;RigidBodyChain&quot;: {
        &quot;RigidBodyTypes&quot;: [
            {
                &quot;path&quot;: &quot;json-input-chains/json-chain-links/20mm-female-four-sided-square.json&quot;
            },
            {
                &quot;path&quot;: &quot;json-input-chains/json-chain-links/20mm-male-four-sided-square.json&quot;
            }
        ],
        &quot;ConnectivityGraph&quot;: {
            &quot;node1&quot;: {
                &quot;body_name&quot;: &quot;20mm-female-four-sided-square&quot;,
                &quot;edges&quot;: [
                    {&quot;target&quot;: &quot;node2&quot;}
                ],
                &quot;configuration&quot;: {
                    &quot;x&quot;: 10.0,
                    &quot;y&quot;: 0.0,
                    &quot;theta&quot;: 0.0
                }
            },
            &quot;node2&quot;: {
                &quot;body_name&quot;: &quot;20mm-male-four-sided-square&quot;,
                &quot;edges&quot;: [
                    {&quot;target&quot;: &quot;node1&quot;}
                ],
                &quot;configuration&quot;: {
                    &quot;x&quot;: 30.5,
                    &quot;y&quot;: 0.0,
                    &quot;theta&quot;: 0.0
                }

            }
        },
        &quot;FixedRigidBodyNode&quot;: &quot;node1&quot;
    }
}</code></pre><ul><li><p><code>&quot;RigidBodyTypes&quot;</code> is the list of each rigid body type. Each rigid body type specifies a boundary of 2D points and is given a name within its json file. <code>path</code> is the disk location of the json file specifying the rigid body type. Boundary coordinates are specified in a local frame where the origin should be roughly in the middle of the boundary.</p></li><li><p><code>&quot;ConnectivityGraph&quot;</code> dictates which rigid bodies may interact with one another. The connectivity graph is a dictionary of vertices in the graph keyed by the human readable name of each vertex. Each vertex contains:</p><ul><li><code>&quot;body_name&quot;</code> The name of the rigid body type whose boundary is used for this vertex</li><li><code>&quot;edges&quot;</code> A list of edges from the current vertex ending in vertices specified by a human readable name</li><li><code>&quot;configuration&quot;</code> The configuration of this rigid body in world coordinates. <code>theta</code> is the angle in radians that the boundary of the rigid body has been rotated about its local frame origin.</li></ul></li><li><p><code>&quot;FixedRigidBodyNode&quot;</code> The name of the vertex in the chain&#39;s graph which is assumed to be fixed. If no node should be fixed, you can make a &quot;ghost&quot; node and fix that. See the flock control example for more on this.</p></li></ul><h2><a class="nav-anchor" id="PNG-1" href="#PNG-1">PNG</a></h2><p>You have the option of specifying a chain as a PNG image where all pixels which are black are used to create a chain with only two types of rigid bodies which are the same shape as a pixel (square).</p><footer><hr/><a class="next" href="reference/"><span class="direction">Next</span><span class="title">Reference</span></a></footer></article></body></html>
