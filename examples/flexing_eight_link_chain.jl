using PyCall
import PyPlot
using PuzzleFlex

@pyimport matplotlib.animation as animation


function flexing_eight_block_animation_example()
    PyPlot.close("all")

    chain = Utils.load_chain_from_file("json-input-chains/eight-link-square-chain.json")

    figure, axis = PyPlot.subplots(figsize=(19.2, 10.8))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/flexing_eight_block_animation.mp4")

    objective_function = LinearFlexibilityAnalysis.construct_weight_vector(
        chain,
        [
            ("node3", GeometryTypes.Point3f0(1.0, 1.0, 0.0)),
            ("node4", GeometryTypes.Point3f0(1.0, 0.0, 0.0))
        ]
    )

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
    initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

    displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
        constraint_jacobian,
        initial_distances,
        objective_function
    )

    frames = seconds * fps
    timestep = 1.0 / frames

    hot_color = [1.0, 0.0, 0.0]
    cold_color = [110.0 / 255.0, 252.0 / 255, 2.0 / 255.0]
    color_cutoff = 1.5

    for i in 1:frames
        axis[:clear]()
        constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        constraint_colors = map(
            evaluation -> begin
                if evaluation < 0
                    color = hot_color
                elseif evaluation > color_cutoff
                    color = cold_color
                else
                    color = Visualization.interpolate_color(cold_color, hot_color, 1 - (evaluation / color_cutoff))
                end

                color
            end,
            constraint_distances
        )

        Visualization.display_chain_links!(axis, chain)
        Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)
        Visualization.surround_chains!(axis, [chain])

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()
end


function eight_block_flex_x_example()
    input_file = "json-input-chains/eight-link-square-chain.json"
    chain = Utils.load_chain_from_file(input_file)

    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(10, 10))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)
    weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(
        chain,
        [
            (top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0)),
        ]
    )

    chain2 = copy(chain)

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)
    jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)

    displacement = LinearFlexibilityAnalysis.solve_with_weights(jacobian, constraint_distances, weight_vector).first
    LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, displacement)

    Visualization.display_chain_links!(axis, chain)
    Visualization.display_connectivity_graph!(axis, chain, vertex_size=9.0)
    Visualization.display_connectivity_graph!(axis, chain2, edge_color=(1.0, 0.0, 0.0), vertex_color=(1.0, 0.0, 0.0), vertex_size=9.0)

    figure[:savefig]("generated-media/eight_block_chain_max_x.pdf", bbox_inches="tight")
    PyPlot.show()
end
