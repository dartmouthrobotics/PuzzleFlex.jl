using PyCall
import PyPlot
import GeometryTypes
@pyimport matplotlib.patches as Patches
@pyimport matplotlib.collections as Collections

using PuzzleFlex

function square_border_edge_case_example()
    input_file = "json-input-chains/single-line-in-square.json"
    chain = Utils.load_chain_from_file(input_file)

    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(20, 10))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    binding_pair = RigidBodyChains.get_binding_pair_from_name(chain, "node1 => node2")
    raw_distance_constraints = LinearFlexibilityAnalysis.construct_raw_distance_constraints(binding_pair, chain)
    raw_distance_constraints = Dict("box_example" => raw_distance_constraints)

    weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(chain,[("node2", GeometryTypes.Point3f0(0.0, 0.0, 1.0)),])

    maximum_error = 1.0
    timestep_iterations = 5
    boundary_color = (0, 0, 0)
    color_step = [255, 0, 0, 55]
    fill_color = (color_step[1] / 255.0, color_step[2] / 255.0, color_step[3] / 255.0, color_step[4] / 255.0)
    edge_width = 2.0

    @time for i in 1:timestep_iterations
        println("Iteration #", i)
        LinearFlexibilityAnalysis.timestep_and_repair!(
            chain,
            raw_distance_constraints,
            maximum_error,
            weight_vector
        )

        if i < timestep_iterations && i > 1 && i%3 != 0 && (i-1)%3 != 0
            color_step[i%3] += 255
            color_step[(i-1)%3] -= 255
            fill_color = (color_step[1] / 255.0, color_step[2] / 255.0, color_step[3] / 255.0, color_step[4] / 255.0)
        elseif i%3 == 0 && i > 1
            color_step[3] += 255
            color_step[2] -= 255
            fill_color = (color_step[1] / 255.0, color_step[2] / 255.0, color_step[3] / 255.0, color_step[4] / 255.0)
        elseif (i-1)%3 == 0 && i > 1
            color_step[1] += 255
            color_step[3] -= 255
            fill_color = (color_step[1] / 255.0, color_step[2] / 255.0, color_step[3] / 255.0, color_step[4] / 255.0)
        elseif i == timestep_iterations
            fill_color=(99 / 255.0, 87 / 255.0, 95 / 255.0)
        end
        
        println(i)
        println(fill_color)
        println("~~~~~~~~~~")

        link_boundary = reduce(vcat, map(transpose, RigidBodyChains.get_rigid_body_boundary(chain, RigidBodyChains.get_node_id_from_name(chain, "node2"))))

        polygon_patches = []
        link_polygon = Patches.Polygon(link_boundary)
        push!(polygon_patches, link_polygon)

        patch_collection = Collections.PatchCollection(polygon_patches)
        patch_collection[:set_color](fill_color)
        patch_collection[:set_edgecolor](boundary_color)

        axis[:add_collection](patch_collection)

        color_step[4] +=50

    end

    edge_lines = []

    for constraints in raw_distance_constraints
        for constraint in constraints.second
            constraint_edge = RigidBodyChains.get_rigid_body_boundary(chain, constraint.edge_node)[collect(constraint.edge)]

            push!(edge_lines, constraint_edge[1] => constraint_edge[2])
        end
    end

    patch_collection_border = Collections.LineCollection(edge_lines, linewidths=edge_width, colors=boundary_color)

    axis[:add_collection](patch_collection_border)

    all_boundary_points = reduce(vcat, reduce(vcat, map(transpose, map(collect, edge_lines))))

    y_max = maximum(all_boundary_points[:, 2])
    y_min = minimum(all_boundary_points[:, 2])

    x_max = maximum(all_boundary_points[:, 1])
    x_min = minimum(all_boundary_points[:, 1])

    axis[:set_xlim]((x_min, x_max))
    axis[:set_ylim]((y_min, y_max))

    figure[:savefig]("generated-media/square_border_edge_case_example.pdf", bbox_inches="tight")
    PyPlot.show()
end
