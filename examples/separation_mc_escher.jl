import PyPlot
import GeometryTypes
import LinearAlgebra

using PuzzleFlex

"""
    separation_mc_escher_birds_example(; save_animation=false)

For mc escher example, try to separate it and save a figure before separation. If save_animation set to false,
a figure after separation will also saved. If save_animation set to true, a bunch of figures and configuration files will be
saved. You can later use test/mc_escher_video_maker/apply_mc_escher_texture.py to generate the video.
"""
function separation_mc_escher_birds_example(; save_animation=false)
    input_file = "json-input-chains/mc-escher-12-birds.json"
    chain = Utils.load_chain_from_file(input_file)

    mc_escher_blue_bird_color = (90/255, 136/255, 191/255, 1)

    fill_color_dict = Dict(node_name => mc_escher_blue_bird_color for node_name in ["node3", "node4", "node7", "node8", "node11", "node12"])

    Visualization.save_with_border("generated-media/separation_mc_escher_before.pdf", chain, fill_color_dict = fill_color_dict)
    PyPlot.show()
    PyPlot.pause(1)

    # Calculate separation displacement.
    separate_displacement = LinearFlexibilityAnalysis.get_separation_displacement(chain, 100)
    # This case might not be able to separate
    if separate_displacement == nothing
        @warn "Case is not separable"
        return
    end


    if ! save_animation
        println("Displaying separated chain")
        result_chain = deepcopy(chain)
        LinearFlexibilityAnalysis.set_chain_to_configuration!(result_chain, separate_displacement.first)
        Visualization.save_with_border("generated-media/separation_mc_escher_after.png",
             result_chain,
             fill_color_dict = fill_color_dict,
        )
    else
        video_length = 800
        result_chain_list = []
        for frame = 1:video_length
            println("Saving animation frame ", frame)
            result_chain = deepcopy(chain)
            LinearFlexibilityAnalysis.set_chain_to_configuration!(result_chain, separate_displacement.first.*(frame/video_length))

            frame_file_name = "generated-media/separation_mc_escher_after_" * lpad(string(frame), 4, "0")*".png"
            Visualization.save_with_border(frame_file_name,
                result_chain,
                fill_color_dict = fill_color_dict
            )

            Visualization.save_configuration_to_json(frame_file_name * ".json", result_chain)
        end
    end
end
