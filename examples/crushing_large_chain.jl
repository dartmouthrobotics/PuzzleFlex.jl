import PyPlot

using PuzzleFlex

function large_chain_crushing_animation_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    midpoint = [500, 600]

    chain = Utils.load_chain_from_file("png-input-chains/50x50-multi-compound-chain.png")

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/crushing_large_chain_animation.mp4")

    frames = seconds * fps
    timestep = 1.5 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()
        objective_function = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
            chain,
            midpoint
        )

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()
end


function crushing_large_open_chain_example()
    # lets do a big open chain (like a 50-link one) and crush it toward its center
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    Visualization.clear_axis_markers!(axis)
    axis[:set_aspect]("equal")

    input_file = "png-input-chains/soda_can_hand_drawn.png"
    chain = Utils.load_chain_from_file(input_file)
    all_chains = [copy(chain)]

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)
    weight_vector = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(chain, [500, 500])

    bottom_right_node_id = RigidBodyChains.get_bottom_right_node_id(chain)
    jacobian_addon, distance_addon = LinearFlexibilityAnalysis.constrain_node_to_be_fixed(chain, bottom_right_node_id)

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    
    try
        LinearFlexibilityAnalysis.timestep_until_convergence!(chain, distance_constraints, 3.0, weight_vector, jacobian_addon=jacobian_addon, distance_addon=distance_addon)
    catch
        println("ERROR during solve. Continuing.")
    end
    push!(all_chains, copy(chain))

    Visualization.display_chain_links!(axis, chain)
    Visualization.display_connectivity_graph!(axis, all_chains[1], edge_color=(1.0, 0.0, 0.0), vertex_color=(1.0, 0.0, 0.0) )
    Visualization.surround_chains!(axis, all_chains)
    figure[:savefig]("generated-media/crushed-can-example-after-crush.pdf", bbox_inches="tight")
end
