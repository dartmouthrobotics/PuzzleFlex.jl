using PuzzleFlex

include("./bad_linear_approximation.jl")
include("./constraint_example.jl")
include("./crushing_large_chain.jl")
include("./fix_extra_block.jl")
include("./flexing_eight_link_chain.jl")
include("./flock_control.jl")
include("./non_convergent_linear_approximation.jl")
include("./performance_table.jl")
include("./tolerance_analysis.jl")
include("./tolerance_analysis_animation.jl")
include("./topology_optimization.jl")
include("./very_large_chain.jl")
include("./separation_mc_escher.jl")

example_functions = [
    "bad_linear_approximation_example",
    "two_block_constraint_example",
    "large_chain_crushing_animation_example",
    "extra_constraint_example",
    "flexing_eight_block_animation_example",
    "eight_block_flex_x_example",
    "flock_control_example",
    "flock_formation_control_animation_example",
    "square_border_edge_case_example",
    "performance_table_example",
    "tolerance_search_example",
    "tolerance_analysis_example",
    "tolerance_analysis_animation_example",
    "exploding_squares_topology_optimization_animation_example",
    "chain_topology_flex_comparrison_example",
    "very_large_chain_example",
    "separation_mc_escher_birds_example"
]

function call_example_function(function_string)
    return getfield(Main, Symbol(function_string))
end
