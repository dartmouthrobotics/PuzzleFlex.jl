using PyCall

import LinearAlgebra

import PyPlot
import JSON
import GeometryTypes
import Distributions
@pyimport matplotlib.animation as animation

using PuzzleFlex


function constrain_motion_of_leader(rogue_squadron, rogue_commander_id, max_displacement)
    rogue_commander_x, rogue_commander_y, _ = LinearFlexibilityAnalysis.get_configuration_variable_indices(
        rogue_squadron,
        rogue_commander_id
    )

    top_left = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))
    top_right = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))
    bottom_left = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))
    bottom_right = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))

    top_right[rogue_commander_x] = -1
    top_right[rogue_commander_y] = -1

    top_left[rogue_commander_x] = 1
    top_left[rogue_commander_y] = -1

    bottom_left[rogue_commander_x] = 1
    bottom_left[rogue_commander_y] = 1

    bottom_right[rogue_commander_x] = -1
    bottom_right[rogue_commander_y] = 1

    dist_supplement = [max_displacement; max_displacement; max_displacement; max_displacement]

    reduce(vcat, [top_right, top_left, bottom_left, bottom_right]), dist_supplement
end


function constraint_thetas(rogue_squadron)
    matrix_rows = []
    dist_rows = []

    max_theta = pi / 4.0

    for node_id in RigidBodyChains.get_all_node_ids(rogue_squadron)
        if node_id != rogue_squadron.fixed_rigid_body_node_id

            _, _, node_theta = LinearFlexibilityAnalysis.get_configuration_variable_indices(rogue_squadron, node_id)
            theta_ub_row = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))
            theta_lb_row = zeros(1, LinearFlexibilityAnalysis.get_number_of_variables(rogue_squadron))

            theta_ub_row[node_theta] = -1
            theta_lb_row[node_theta] = 1

            dist_supplement = [max_theta, max_theta]
            append!(dist_rows, dist_supplement)
            append!(matrix_rows, [theta_ub_row, theta_lb_row])

            # t <= pi
            # t - pi <= 0
            # -t + pi >= 0
            #
            # t >= -pi
            # t + pi >= 0
        end
    end

    reduce(vcat, matrix_rows), reduce(vcat, dist_rows)
end


function construct_flock_separation_weight_vector(flock, flock_json, commander_id)
    commander_x = RigidBodyChains.get_configuration(flock, commander_id)[1]

    node_weights = [
        ("rogue-commander", GeometryTypes.Point3f0(0.0, 1.0, 0.0))
    ]

    for node_id in RigidBodyChains.get_all_unfixed_node_ids(flock)
        node_name = RigidBodyChains.get_node_name(flock, node_id)

        if node_name == "rogue-commander"
            continue
        end

        node_configuration = RigidBodyChains.get_configuration(flock, node_id)

        parent_name = flock_json["RigidBodyChain"]["ConnectivityGraph"][node_name]["edges"][1]["target"]
        parent_x = flock_json["RigidBodyChain"]["ConnectivityGraph"][parent_name]["configuration"]["x"]

        node_direction = GeometryTypes.Point3f0(1.0, 0.0, 0.0)

        if node_configuration[1] <= parent_x
            node_direction = GeometryTypes.Point3f0(-1.0, 0.0, 0.0)
        end

        push!(node_weights, (node_name, node_direction))
    end

    LinearFlexibilityAnalysis.construct_weight_vector(flock, node_weights)
end


function flock_control_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(3, 3))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)
    number_frames = 3

    rogue_squadron, rogue_squadron_dict = construct_flock(1024)

    visibility_constraints = construct_visibility_constraints_for_robot_flock(rogue_squadron_dict, rogue_squadron)

    Visualization.display_robot_flock!(axis, rogue_squadron)
    all_chains = [copy(rogue_squadron)]
    Visualization.surround_chains!(axis, all_chains)
    figure[:savefig]("generated-media/1024-robot-flock-before-optimization.pdf", bbox_inches="tight")
    axis[:clear]()

    rogue_commander_id = RigidBodyChains.get_node_id_from_name(rogue_squadron, "rogue-commander")

    Visualization.surround_chains!(axis, all_chains)

    separation_objective = construct_flock_separation_weight_vector(rogue_squadron, rogue_squadron_dict, rogue_commander_id)

    for i in 1:number_frames
        println("Iteration ", i)

        collision_constraints = construct_collision_constraints_for_robot_flock(rogue_squadron)
        distance_constraints = Dict("constraints" => vcat(visibility_constraints, collision_constraints))

        constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(rogue_squadron, distance_constraints)

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(rogue_squadron, distance_constraints)

        jac_supplement, dist_supplement = constrain_motion_of_leader(rogue_squadron, rogue_commander_id, 1.0)

        constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
        constraint_distances = vcat(constraint_distances, dist_supplement)

        jac_supplement, dist_supplement = constraint_thetas(rogue_squadron)

        constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
        constraint_distances = vcat(constraint_distances, dist_supplement)

        soln = LinearFlexibilityAnalysis.solve_with_weights(constraint_jacobian, constraint_distances, separation_objective)

        time_step = LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(rogue_squadron, soln.first, 0.01, distance_constraints; timestep_spacing=0.001)
        println(time_step)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(rogue_squadron, time_step * soln.first)
        all_chains = [copy(rogue_squadron)]
    end


    Visualization.display_robot_flock!(axis, rogue_squadron)
    Visualization.surround_chains!(axis, all_chains)
    figure[:savefig]("generated-media/1024-robot-flock-after-separation.pdf", bbox_inches="tight")

    println("Constricting")
    for i in 1:20
        commander_configuration = RigidBodyChains.get_configuration(rogue_squadron, rogue_commander_id) 
        constriction_objective = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_vertical_line(
            rogue_squadron,
            commander_configuration[1]
        )

        collision_constraints = construct_collision_constraints_for_robot_flock(rogue_squadron)
        distance_constraints = Dict("constraints" => vcat(visibility_constraints, collision_constraints))

        constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(rogue_squadron, distance_constraints)

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(rogue_squadron, distance_constraints)

        jac_supplement, dist_supplement = constrain_motion_of_leader(rogue_squadron, rogue_commander_id, 1.0)

        constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
        constraint_distances = vcat(constraint_distances, dist_supplement)

        jac_supplement, dist_supplement = constraint_thetas(rogue_squadron)

        constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
        constraint_distances = vcat(constraint_distances, dist_supplement)

        soln = LinearFlexibilityAnalysis.solve_with_weights(constraint_jacobian, constraint_distances, constriction_objective)

        time_step = LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(rogue_squadron, soln.first, 0.00, distance_constraints; timestep_spacing=0.001)
        time_step = min(time_step, 0.05)
        println(time_step)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(rogue_squadron, time_step * soln.first)
        all_chains = [copy(rogue_squadron)]
    end

    axis[:clear]()
    Visualization.display_robot_flock!(axis, rogue_squadron)
    Visualization.surround_chains!(axis, all_chains)
    figure[:savefig]("generated-media/1024-robot-flock-after-compaction.pdf", bbox_inches="tight")

    PyPlot.show()
end

function construct_flock(desired_number_bots)
    constructed_flock = JSON.parse(read(open("json-input-chains/commander-only.json"), String))

    leader_location = GeometryTypes.Point3f0(100, 100, 0)
    left_child_offset = [-3.5, -9.5]
    right_child_offset = [3.5, -9.5]

    nodes_without_left_child = ["rogue-commander"]
    nodes_without_right_child = ["rogue-commander"]

    number_nodes = 1

    added_configurations = []

    while number_nodes < desired_number_bots
        is_left_child = rand([true, false])

        if is_left_child
            node_list = nodes_without_left_child
        else
            node_list = nodes_without_right_child
        end

        new_parent = rand(node_list)
        deleteat!(node_list, findall(isequal(new_parent), node_list))

        offset = right_child_offset

        if is_left_child
            offset = left_child_offset
        end

        parent_configuration = constructed_flock["RigidBodyChain"]["ConnectivityGraph"][new_parent]["configuration"]

        new_squadmate_configuration = Dict()

        if is_left_child
            new_squadmate_configuration = Dict(
                "x" => parent_configuration["x"] + left_child_offset[1],
                "y" => parent_configuration["y"] + left_child_offset[2],
                "theta" => 0.0
            )
        else
            new_squadmate_configuration = Dict(
                "x" => parent_configuration["x"] + right_child_offset[1],
                "y" => parent_configuration["y"] + right_child_offset[2],
                "theta" => 0.0
            )
        end
        configuration_tuple = (new_squadmate_configuration["x"], new_squadmate_configuration["y"], new_squadmate_configuration["theta"])

        skip_node_branch = rand(Distributions.Uniform(0.0, 1.0))

        if length(nodes_without_left_child) == 0 || length(nodes_without_right_child) == 0
            skip_node_branch = 1.0
        end

        if !in(configuration_tuple, added_configurations) && skip_node_branch > 0.00001
            new_squadmate_name = "node$(number_nodes)"
            new_squadmate = Dict(
                "body_name" => "triangle-and-origin",
                "edges" => [Dict(
                    Dict("target" => new_parent)
                )],
                "configuration" => new_squadmate_configuration
            )

            push!(nodes_without_left_child, new_squadmate_name)
            push!(nodes_without_right_child, new_squadmate_name)
            number_nodes += 1

            constructed_flock["RigidBodyChain"]["ConnectivityGraph"][new_squadmate_name] = new_squadmate

            push!(
                added_configurations,
                configuration_tuple
            )
        end
    end

    RigidBodyChains.RigidBodyChain(constructed_flock), constructed_flock
end


function construct_visibility_constraints_for_robot_flock(flock_json::Dict, flock_chain::RigidBodyChains.RigidBodyChain)
    # for each we get a parent and make a visibility constraint that their triangle contains their parent
    visibility_constraints = []

    for node_id in RigidBodyChains.get_all_node_ids(flock_chain)
        node_name = RigidBodyChains.get_node_name(flock_chain, node_id)

        if node_name == "rogue-commander" || node_name == "ghost-origin"
            continue
        end

        parent_name = flock_json["RigidBodyChain"]["ConnectivityGraph"][node_name]["edges"][1]["target"]
        parent_id = RigidBodyChains.get_node_id_from_name(flock_chain, parent_name)

        node_visibility_constraints = [
            LinearFlexibilityAnalysis.DistanceConstraint(node_id, parent_id, (7, 8), 1),
            LinearFlexibilityAnalysis.DistanceConstraint(node_id, parent_id, (8, 9), 1),
            LinearFlexibilityAnalysis.DistanceConstraint(node_id, parent_id, (9, 10), 1),
        ]

        append!(visibility_constraints, node_visibility_constraints)
    end

    visibility_constraints
end


function construct_collision_constraints_for_robot_flock(flock_chain)
    collision_constraints = []
    constrained_pairs = []
    number_nearest_neighbors = 4

    for node_id in RigidBodyChains.get_all_node_ids(flock_chain)
        if node_id == flock_chain.fixed_rigid_body_node_id
            continue
        end

        current_node_configuration = RigidBodyChains.get_configuration(flock_chain, node_id)
        current_node_boundary = RigidBodyChains.get_rigid_body_boundary(flock_chain, node_id)

        neighbors_sorted_by_distance = sort(
            filter(neighbor_id -> node_id != neighbor_id && neighbor_id != flock_chain.fixed_rigid_body_node_id, RigidBodyChains.get_all_node_ids(flock_chain)),
            by=neighbor_id -> begin
               neighbor_configuration = RigidBodyChains.get_configuration(flock_chain, neighbor_id)
               LinearAlgebra.norm(current_node_configuration[1:2] - neighbor_configuration[1:2])
            end
        )

        nearest_neighbors = neighbors_sorted_by_distance[1:min(number_nearest_neighbors, length(neighbors_sorted_by_distance))]

        for neighbor in nearest_neighbors
            if !(in((neighbor, node_id), constrained_pairs) || in((node_id, neighbor), constrained_pairs))
                square_edges = [
                    (2, 3),
                    (3, 4),
                    (4, 5),
                    (5, 6)
                ]

                neighbor_boundary = RigidBodyChains.get_rigid_body_boundary(flock_chain, neighbor)
                edge_node = neighbor
                point_node = node_id

                for point_index in 2:6
                    for edge in square_edges
                        push!(collision_constraints, LinearFlexibilityAnalysis.DistanceConstraint(edge_node, point_node, edge, point_index))
                    end
                end

                push!(constrained_pairs, (node_id, neighbor))
            end
        end
    end

    filter(constraint -> LinearFlexibilityAnalysis.constraint_is_satisfied(flock_chain, constraint, 0.01), collision_constraints)
end

function flock_formation_control_animation_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(19.2, 10.8))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    rogue_squadron, rogue_squadron_dict = construct_flock(512)

    visibility_constraints = construct_visibility_constraints_for_robot_flock(rogue_squadron_dict, rogue_squadron)

    fps = 14
    seconds = 6

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/flock_control_separation.mp4")

    frames = seconds * fps

    rogue_commander_id = RigidBodyChains.get_node_id_from_name(rogue_squadron, "rogue-commander")
    separation_objective = construct_flock_separation_weight_vector(rogue_squadron, rogue_squadron_dict, rogue_commander_id)
    timestep = 0.005

    soln = nothing
    distance_constraints = nothing
    time_step = 0.05

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        if i == 1 || time_step == 0.0
            println("recomputing direction")
            collision_constraints = construct_collision_constraints_for_robot_flock(rogue_squadron)
            distance_constraints = Dict("constraints" => vcat(visibility_constraints, collision_constraints))

            constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(rogue_squadron, distance_constraints)

            constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(rogue_squadron, distance_constraints)

            jac_supplement, dist_supplement = constrain_motion_of_leader(rogue_squadron, rogue_commander_id, 1.0)

            constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
            constraint_distances = vcat(constraint_distances, dist_supplement)

            jac_supplement, dist_supplement = constraint_thetas(rogue_squadron)

            constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
            constraint_distances = vcat(constraint_distances, dist_supplement)

            soln = LinearFlexibilityAnalysis.solve_with_weights(constraint_jacobian, constraint_distances, separation_objective)
        end

        time_step = LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(rogue_squadron, soln.first, 0.01, distance_constraints; timestep_spacing=0.01)
        time_step = min(timestep, time_step)
        println("timestep", time_step)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(rogue_squadron, time_step * soln.first)
        all_chains = [copy(rogue_squadron)]

        Visualization.display_robot_flock!(axis, rogue_squadron)
        #Visualization.surround_chains!(axis, all_chains)

        movie_writer[:grab_frame]()
    end

    timestep /= 2.0
    midpoint = [70, -150]
    numzeros = 0

    for i in 1:frames*2
        println("capturing frame ", i)
        axis[:clear]()

        objective = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(rogue_squadron, midpoint)

        if i == 1 || time_step == 0.0
            numzeros += 1
            println("recomputing direction")
            collision_constraints = construct_collision_constraints_for_robot_flock(rogue_squadron)
            distance_constraints = Dict("constraints" => vcat(visibility_constraints, collision_constraints))

            constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(rogue_squadron, distance_constraints)

            constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(rogue_squadron, distance_constraints)

            jac_supplement, dist_supplement = constrain_motion_of_leader(rogue_squadron, rogue_commander_id, 1.0)

            constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
            constraint_distances = vcat(constraint_distances, dist_supplement)

            jac_supplement, dist_supplement = constraint_thetas(rogue_squadron)

            constraint_jacobian = vcat(constraint_jacobian, jac_supplement)
            constraint_distances = vcat(constraint_distances, dist_supplement)

            soln = LinearFlexibilityAnalysis.solve_with_weights(constraint_jacobian, constraint_distances, objective)
        end

        time_step = LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(rogue_squadron, soln.first, 0.01, distance_constraints; timestep_spacing=0.001)
        time_step = min(timestep, time_step)
        println("timestep", time_step)

        if time_step > 0
            numzeros = 0
        end

        LinearFlexibilityAnalysis.set_chain_to_configuration!(rogue_squadron, time_step * soln.first)
        all_chains = [copy(rogue_squadron)]

        Visualization.display_robot_flock!(axis, rogue_squadron)
        axis[:scatter]([midpoint[1]], [midpoint[2]])
        #Visualization.surround_chains!(axis, all_chains)

        movie_writer[:grab_frame]()

        if numzeros > 5
            break
        end
    end

    movie_writer[:finish]()
end
