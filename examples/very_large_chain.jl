import PyPlot

using PuzzleFlex

function very_large_chain_example()
    chain = Utils.load_chain_from_file("png-input-chains/50x50-multi-compound-chain-less-sparse.png")

    unmoved_chain = copy(chain)

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    max_error = 1.0

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)
    weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(chain, [(top_right_node_name, GeometryTypes.Point3f0(0.0, 1.0, 0.0))]) 

    LinearFlexibilityAnalysis.timestep_until_convergence!(chain, distance_constraints, max_error, weight_vector, verbose=true)

    fig, ax = PyPlot.subplots()
    ax[:set_aspect]("equal")
    Visualization.clear_axis_markers!(ax)

    Visualization.display_chain_links!(ax, chain)
    Visualization.display_connectivity_graph!(ax, unmoved_chain, vertex_color=(1.0, 0.0, 0.0, 0.7), edge_color=(1.0, 0.0, 0.0, 0.7), vertex_size=0.2)
    Visualization.surround_chains!(ax, [chain, unmoved_chain])

    fig[:savefig]("generated-media/very-large-chain-example.pdf", bbox_inches="tight")
end
