import PyPlot
using PuzzleFlex

function extra_constraint_example()
    input_file = "png-input-chains/skyline-topo-1.png"
    chain = Utils.load_chain_from_file(input_file)

    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(10, 10))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)

    weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(chain, [(top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0))])

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    constraint_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)
    jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)

    node_id = RigidBodyChains.get_node_id_from_name(chain, RigidBodyChains.get_bottom_right_node_name(chain))

    constraint_matrix_addition, zeros_matrix = LinearFlexibilityAnalysis.constrain_node_to_be_fixed(chain, node_id)

    new_jacobian = reduce(vcat, [jacobian, constraint_matrix_addition])

    new_constraint_distances = reduce(vcat, [constraint_distances, zeros_matrix])

    displacement = LinearFlexibilityAnalysis.solve_with_weights(new_jacobian, new_constraint_distances, weight_vector).first
    LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, displacement)

    Visualization.display_chain_links!(axis, chain)
    Visualization.display_connectivity_graph!(axis, chain, vertex_size=9.0)

    axis[:clear]()
    figure[:savefig]("generated-media/extra_constraint.pdf", bbox_inches="tight")
    PyPlot.show()
end
