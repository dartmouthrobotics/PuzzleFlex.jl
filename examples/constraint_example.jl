import PyPlot
using PuzzleFlex

function two_block_constraint_example()
    input_file = "json-input-chains/two-link-open-chain.json"
    chain = Utils.load_chain_from_file(input_file)

    PyPlot.close("all")
    figure, axis = PyPlot.subplots(figsize=(2, 3))
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)

    Visualization.display_chain_links!(axis, chain, fill_color=Dict("node1" => Visualization.WARM_GREY, "node2" => Visualization.MINT_GREEN))
    Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_size=5.0, edge_width=0.6, v_width=0.6, point_color=((0.0, 0.0, 0.0)))
    Visualization.surround_chains!(axis, [chain])

    figure[:savefig]("generated-media/two_block_constraint_example.pdf", bbox_inches="tight")
    PyPlot.show()
end
