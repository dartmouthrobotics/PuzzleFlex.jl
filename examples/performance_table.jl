import Dates

import PyPlot

using PuzzleFlex

function performance_table_example()
    # what is an objective here? Lets do moving the top right block to the left 

    table_chains = [
        "png-input-chains/soda_can_hand_drawn.png",
        "png-input-chains/50-link-open-chain.png",
        "png-input-chains/50x50-multi-compound-chain.png",
        "png-input-chains/skyline-topo-1.png",
        "png-input-chains/skyline-topo-6.png",
        "png-input-chains/10x10-open-ring.png",
        "png-input-chains/single-skyscraper.png",
        "png-input-chains/100x100-big-empty-square.png",
        "png-input-chains/100x100-big-empty-square-with-vertical-and-horizontal-crossbar.png",
        "png-input-chains/50x50-multi-compound-chain-less-sparse.png",
        "png-input-chains/100x100-big-empty-square-with-mostest-crossbars.png",
    ]

    table_column_headers = [
        "Rigid Bodies",
        "Jacobian Size",
        "Iterations",
        "Runtime (seconds)"
    ]

    table_row_data = []

    for chain_file in table_chains
        chain = Utils.load_chain_from_file(chain_file)

        weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(
            chain,
            [(RigidBodyChains.get_top_right_node_name(chain), GeometryTypes.Point3f0(0.0, 1.0, 0.0))]
        )

        distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
        jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)

        run_start_time = Dates.now()

        number_of_iterations, _ = LinearFlexibilityAnalysis.timestep_until_convergence!(
            chain,
            distance_constraints,
            0.0,
            weight_vector
        )

        run_end_time = Dates.now()

        runtime_miliseconds = Dates.value(run_end_time - run_start_time)
        runtime_seconds = convert(Float64, runtime_miliseconds) / 1000.0

        push!(
            table_row_data,
            [
                RigidBodyChains.get_number_of_rigid_bodies(chain),
                "$(size(jacobian)[1]) x $(size(jacobian)[2])",
                number_of_iterations,
                runtime_seconds
            ]
        )

        # format as a latex table 
        println("")
        sort!(table_row_data, by=row_data -> row_data[1])

        header = join(table_column_headers, " & ") * " \\\\"
        rows = map(row_data -> join(row_data, " & ") * " \\\\", table_row_data)        

        println(header * "\n" * join(rows, "\n"))
    end

    # format as a latex table 
    header = join(table_column_headers, " & ") * " \\\\"
    rows = map(row_data -> join(row_data, " & ") * " \\\\", table_row_data)        

    println(header * "\n" * join(rows, "\n"))
end
