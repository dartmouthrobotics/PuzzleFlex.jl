using PyCall
import PyPlot
@pyimport matplotlib.animation as animation

using PuzzleFlex

function exploding_squares_topology_optimization_animation_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    midpoint = [1000, 1000]

    chain = Utils.load_chain_from_file("png-input-chains/100x100-big-empty-square.png")

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/topology_optimization_open_square.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    objective_function = -LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()

    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    chain = Utils.load_chain_from_file("png-input-chains/100x100-big-empty-square-with-vertical-and-horizontal-crossbar.png")

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/topology_optimization_crossbar_square.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)

    objective_function = -LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()

    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    chain = Utils.load_chain_from_file("png-input-chains/100x100-big-empty-square-with-vertical-crossbar.png")

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/topology_optimization_vertical_crossbar_square.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)

    objective_function = -LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()
end


function chain_topology_flex_comparrison_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")

    chains = [
        Utils.load_chain_from_file("png-input-chains/100x100-big-empty-square.png"),
        Utils.load_chain_from_file("png-input-chains/100x100-big-empty-square-with-vertical-crossbar.png")
    ]

    reverse!(chains)

    cold_color = [0.0, 0.0, 1.0]
    hot_color = [1.0, 0.0, 0.0]

    for (index, chain) in enumerate(chains)
        axis[:clear]()
        Visualization.clear_axis_markers!(axis)

        distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
        weight_vector = -1 * LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
            chain,
            [1000, 1000]
        )
        color = (hot_color - cold_color) * index / length(chains) + cold_color

        try
            LinearFlexibilityAnalysis.timestep_until_convergence!(chain, distance_constraints, 0.01, weight_vector, min_gain=1.1)
        catch
            print("Error during optimization, continuing")
        end

        Visualization.display_chain_links!(axis, chain, boundary_width=0.5)
        Visualization.surround_chains!(axis, [chain])
        figure[:savefig]("topology_optimization_example_$(index).pdf", bbox_inches="tight")
    end
    PyPlot.show()
end
