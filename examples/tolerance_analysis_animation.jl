using PyCall
import PyPlot
@pyimport matplotlib.animation as animation

using PuzzleFlex


function tolerance_analysis_animation_example()
    # this should show them both clinching down. P easy
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    midpoint = [500, 600]
    offset = 0.040

    chain = Utils.load_chain_from_file("png-input-chains/soda_can_hand_drawn.png")
    chain = RigidBodyChains.get_chain_with_offset_boundaries(
        chain,
        offset
    )

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/tolerance_analysis_tight_can.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)

    objective_function = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()

    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    midpoint = [500, 600]

    offset = -0.22

    chain = Utils.load_chain_from_file("png-input-chains/soda_can_hand_drawn.png")
    chain = RigidBodyChains.get_chain_with_offset_boundaries(
        chain,
        offset
    )

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/tolerance_analysis_loose_can.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    objective_function = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()

    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    midpoint = [500, 600]

    offset = -0.11

    chain = load_chain_from_file("png-input-chains/soda_can_hand_drawn.png")
    chain = RigidBodyChains.get_chain_with_offset_boundaries(
        chain,
        offset
    )

    fps = 14
    seconds = 10

    movie_writer = animation.FFMpegWriter(fps=fps) 
    movie_writer[:setup](figure, "generated-media/tolerance_analysis_mid_loose_can.mp4")

    frames = seconds * fps
    timestep = 1.0 / frames

    distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    objective_function = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(
        chain,
        midpoint
    )

    for i in 1:frames
        println("capturing frame ", i)
        axis[:clear]()

        constraint_jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, distance_constraints)
        initial_distances = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, distance_constraints)

        displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(
            constraint_jacobian,
            initial_distances,
            objective_function
        )

        frame_timestep = min(
            timestep,
            LinearFlexibilityAnalysis.find_maximum_time_step_with_sampling(
                chain,
                displacement,
                0.0,
                distance_constraints
            )
        )

        Visualization.display_chain_links!(axis, chain, boundary_width=0.7)
        #Visualization.display_distance_constraints!(axis, chain, distance_constraints, point_color=[0.0, 0.0, 0.0, 0.0], edge_color=[0.0, 0.0, 0.0, 0.0], v_color=constraint_colors, v_width=2.0)

        LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, frame_timestep * displacement)

        movie_writer[:grab_frame]()
    end

    movie_writer[:finish]()
end
