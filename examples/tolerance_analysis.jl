import PyPlot
import GeometryTypes
import LinearAlgebra

using PuzzleFlex


function tolerance_search_example()
    PyPlot.close("all")

    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    input_file = "png-input-chains/single-skyscraper.png"
    chain = Utils.load_chain_from_file(input_file)

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)

    max_x_displacement = 60
    max_offset = 0.04
    min_offset = -0.4
    offset_sample_spacing = 0.01

    offsets = reverse(collect(min_offset:offset_sample_spacing:max_offset))

    left_index = 1
    right_index = length(offsets)

    max_valid_offset_index = typemin(Int64)
    most_offset_chain = nothing
    offset_chains = []
    all_distance_constraints = []

    cold_color = [0.0, 0.0, 1.0]
    hot_color = [1.0, 0.0, 0.0]

    for (index, offset) in enumerate(offsets)
        offset_chain = RigidBodyChains.get_chain_with_offset_boundaries(chain, offset)
        distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(offset_chain)

        weight_vector = LinearFlexibilityAnalysis.construct_weight_vector(
            offset_chain,
            [(top_right_node_name, GeometryTypes.Point3f0(0.0, 1.0, 0.0))]
        )

        _, displacement = LinearFlexibilityAnalysis.timestep_until_convergence!(offset_chain, distance_constraints, 0.03, weight_vector, min_gain=0.5)

        objective_result = LinearAlgebra.dot(weight_vector, displacement)

        println("top right x ", RigidBodyChains.get_configuration(offset_chain, RigidBodyChains.get_node_id_from_name(chain, top_right_node_name)))
        println(offset, " ", objective_result)

        push!(offset_chains, offset_chain)
        push!(all_distance_constraints, distance_constraints)

        if objective_result < max_x_displacement
            most_offset_chain = offset_chain
            max_valid_offset_index = offset
        else
            break
        end
    end

    for (index, offset_chain) in enumerate(offset_chains)
        color = (hot_color - cold_color) * index / length(offset_chains) + cold_color
        Visualization.display_chain_links!(axis, offset_chain, fill_color=(color..., 0.5), boundary_color=(0.0, 0.0, 0.0, 0.5), boundary_width=0.5)
    end

    Visualization.surround_chains!(axis, offset_chains)
    figure[:savefig]("generated-media/skyscraper-loosening-max-flex-example.pdf", bbox_inches="tight")
    println("Max offset", max_valid_offset_index)
end


function tolerance_analysis_example()
    PyPlot.close("all")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)


    outset = 0.040
    original_chain = RigidBodyChains.get_chain_with_offset_boundaries(
        Utils.load_chain_from_file("png-input-chains/soda_can_hand_drawn.png"),
        outset
    )

    unmoved_chain = copy(original_chain) 

    offset = -0.22
    loosened_chain = RigidBodyChains.get_chain_with_offset_boundaries(
        Utils.load_chain_from_file("png-input-chains/soda_can_hand_drawn.png"),
        offset
    )

    loose_chain_distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(loosened_chain)
    tight_chain_distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(original_chain)

    tight_chain_weights = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(original_chain, [500, 500])
    loose_chain_weights = LinearFlexibilityAnalysis.construct_weight_vector_moving_all_bodies_toward_point(loosened_chain, [500, 500])

    tight_bottom_right_node_id = RigidBodyChains.get_bottom_right_node_id(original_chain)
    loose_bottom_right_node_id = RigidBodyChains.get_bottom_right_node_id(loosened_chain)

    loose_jacobian_addon, loose_distance_addon = LinearFlexibilityAnalysis.constrain_node_to_be_fixed(loosened_chain, loose_bottom_right_node_id)
    tight_jacobian_addon, tight_distance_addon = LinearFlexibilityAnalysis.constrain_node_to_be_fixed(original_chain, tight_bottom_right_node_id)

    try
        LinearFlexibilityAnalysis.timestep_until_convergence!(
            loosened_chain,
            loose_chain_distance_constraints,
            1.5,
            loose_chain_weights,
            timestep_spacing=0.001,
            jacobian_addon=loose_jacobian_addon,
            distance_addon=loose_distance_addon
        )
    catch e
        println("Error during optimization for loose chain. continuing")
    end

    try
        LinearFlexibilityAnalysis.timestep_until_convergence!(
            original_chain,
            tight_chain_distance_constraints,
            1.5,
            tight_chain_weights,
            timestep_spacing=0.001,
            jacobian_addon=tight_jacobian_addon,
            distance_addon=tight_distance_addon
        )
    catch e
        println("Error during optimization for tight chain. continuing")
    end

    Visualization.display_chain_links!(axis, original_chain)
    Visualization.display_connectivity_graph!(axis, unmoved_chain, vertex_color=(1.0, 0.0, 0.0), edge_color=(1.0, 0.0, 0.0))
    Visualization.surround_chains!(axis, [original_chain, unmoved_chain, loosened_chain])
    figure[:savefig]("generated-media/tolerance-analysis-tight-chain-example.pdf", bbox_inches="tight")
    axis[:clear]()

    Visualization.display_chain_links!(axis, loosened_chain)
    Visualization.display_connectivity_graph!(axis, unmoved_chain, vertex_color=(1.0, 0.0, 0.0), edge_color=(1.0, 0.0, 0.0))
    Visualization.surround_chains!(axis, [original_chain, unmoved_chain, loosened_chain])
    figure[:savefig]("generated-media/tolerance-analysis-loose-chain-example.pdf", bbox_inches="tight")
end
