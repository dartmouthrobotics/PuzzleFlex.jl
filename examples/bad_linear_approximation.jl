#import Main.Utils
#import Main.LinearFlexibilityAnalysis
#import Main.RigidBodyChains
using PuzzleFlex

function bad_linear_approximation_example()
    chain = Utils.load_chain_from_file("png-input-chains/skyline-topo-1.png")
    figure, axis = PyPlot.subplots()
    axis[:set_aspect]("equal")
    Visualization.clear_axis_markers!(axis)

    top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)
    objective = LinearFlexibilityAnalysis.construct_weight_vector(chain, [(top_right_node_name, GeometryTypes.Point3f0(1.0, 1.0, 0.0))])

    constraints = LinearFlexibilityAnalysis.construct_distance_constraints(chain)
    jacobian = LinearFlexibilityAnalysis.construct_constraint_jacobian(chain, constraints)
    constraint_dists = LinearFlexibilityAnalysis.get_initial_constraint_distances(chain, constraints)

    displacement, _ = LinearFlexibilityAnalysis.solve_with_weights(jacobian, constraint_dists, objective)

    LinearFlexibilityAnalysis.set_chain_to_configuration!(chain, displacement)
end
