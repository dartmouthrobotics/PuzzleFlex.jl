echo "Building docs..."
julia --project docs/make.jl

echo "Clearing web directory"
mkdir -p ./public
rm -rf ./public/*

echo "Copying docs to web directory"
cp -r docs/build/* public/
