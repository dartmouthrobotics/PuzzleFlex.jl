import JSON
import Main.RigidBodyChains: RigidBodyChain, load_rigid_body_chain_from_json

@testset "RigidBodyChainsTests" begin
    @testset "load_rigid_body_chain_from_json" begin
        open("test/data/simple_rigid_body_chain.json") do simple_chain_file 
            contents = read(simple_chain_file, String)
            parsed_content = JSON.parse(contents)
            chain = load_rigid_body_chain_from_json(parsed_content)

            @test length(keys(chain.connectable_rigid_bodies)) == 2
        end
    end
end
