using Documenter
using PuzzleFlex

makedocs(
    sitename="PuzzleFlex.jl",
    modules=[
        LinearFlexibilityAnalysis,
        RigidBodies,
        ConnectableRigidBodies,
        RigidBodyChains,
        Utils,
        Geometry,
        PNGChainLoader,
        Visualization
    ],
    pages=[
        "index.md",
        "reference.md"
    ],
    repo="https://gitlab.com/dartmouthrobotics/PuzzleFlex.jl"
)
