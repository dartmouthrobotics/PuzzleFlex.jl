# Reference

# Module RigidBodyChains

```@meta
CurrentModule = PuzzleFlex
```

```@docs
RigidBodyChains
```

## Contents 

```@meta
CurrentModule = PuzzleFlex.RigidBodyChains
```

```@docs
RigidBodyChain
```

```@docs
get_node_name
```

```@docs
get_node_id_from_name
```

```@docs
get_all_node_ids
```

```@docs
get_chain_with_offset_boundaries
```

```@docs
get_all_unfixed_node_ids
```

```@docs
get_binding_pair_from_name
```

```@docs
neighbors
```

```@docs
get_number_of_rigid_bodies
```

```@docs
get_configuration
```

```@docs
output_as_text_file 
```

```@docs
set_configuration!
```

```@docs
get_binding_pair
```

```@docs
get_rigid_body_boundary
```

```@docs
get_all_binding_pairs
```

```@docs
load_from_json
```

```@docs
get_bottom_right_node_name
```

```@docs
get_all_backward_binding_pairs
```

```@docs
get_top_right_node_name
```


# Module LinearFlexibilityAnalysis

## Contents

# Module BindingPairs

## Contents

# Module Geometry

## Contents

# Module RigidBodies

## Contents

# Module Visualization

## Contents

# Module PNGChainLoader

## Contents

# Module Visualization

## Contents

# Module RigidBodies

## Contents

# Module Utils

## Contents
