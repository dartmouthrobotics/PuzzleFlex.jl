# Home

![header](assets/constrained-chain.png)

PuzzleFlex.jl provides a way to create a graph representation of an interconnected chain of rigid bodies through the `RigidBodyChain` struct from the `RigidBodyChains` module. The `LinearFlexibilityAnalysis` module provides a way of translating a `RigidBodyChain` to a linear program which can be utilized to find extreme configurations of large chains in a variety of directions in configuration space. The linear program finds a displacement `D` in the configuration space of the chain which maximizes the product `DC` where `C` is a desired configuration in configuration space.  

For example, the following code moves the top right rigid body of the nine link grid specified from a JSON file as far as possible to the right.

```julia
using PuzzleFlex
import GeometryTypes

# sets the maximum error used for selecting time steps
# between solutions of the linear program
maximum_error = 0.0

chain = Utils.load_chain_from_file("json-input-chains/nine-link-grid.json")

# distance constraints are linear distance constraints between edges
# and points of connected rigid bodies
distance_constraints = LinearFlexibilityAnalysis.construct_distance_constraints(
    chain
)

# each node is given a human readable name
top_right_node_name = RigidBodyChains.get_top_right_node_name(chain)

# construct a cost function for the linear program which moves the top right
# node of the chain as much as possible to the right
objective_vector = LinearFlexibilityAnalysis.construct_weight_vector(
    chain,
    [
        (top_right_node_name, GeometryTypes.Point3f0(1.0, 0.0, 0.0))
    ]
)

# repeatedly solves the linear program constructed from
# distance_constraints and objective_vector
# while limiting the error introduced to below maximum_error
LinearFlexibilityAnalysis.timestep_until_convergence!(
    chain,
    distance_constraints,
    maximum_error,
    objective_vector
)
```


# Installing

Run `] add https://gitlab.com/dartmouthrobotics/PuzzleFlex.jl` from the julia repl.

If you do not have [gurobi](http://www.gurobi.com/) with the proper enviornment variables set up
for [Gurobi.jl](https://github.com/JuliaOpt/Gurobi.jl), you will see an error during the installation. The installation
will still work, but the [Clp](https://github.com/JuliaOpt/Clp.jl) solver will be used as a backup. The gurobi solver
generally scales better than the Clp solver, but can be difficult to get your hands on.

# Code Organization



# Specifying a Chain

Currently, a rigid body chain can be specified with either a Dict (parsed from a json file) or a PNG image.

## JSON

The JSON format for specifying a chain allows more flexibility over the PNG image, but requires more manual intervention. Several examples of the format can be found in the `json-input-chains` directory. An example of the format is shown below. The purpose of each key is explained below.

```json
{
    "RigidBodyChain": {
        "RigidBodyTypes": [
            {
                "path": "json-input-chains/json-chain-links/20mm-female-four-sided-square.json"
            },
            {
                "path": "json-input-chains/json-chain-links/20mm-male-four-sided-square.json"
            }
        ],
        "ConnectivityGraph": {
            "node1": {
                "body_name": "20mm-female-four-sided-square",
                "edges": [
                    {"target": "node2"}
                ],
                "configuration": {
                    "x": 10.0,
                    "y": 0.0,
                    "theta": 0.0
                }
            },
            "node2": {
                "body_name": "20mm-male-four-sided-square",
                "edges": [
                    {"target": "node1"}
                ],
                "configuration": {
                    "x": 30.5,
                    "y": 0.0,
                    "theta": 0.0
                }

            }
        },
        "FixedRigidBodyNode": "node1"
    }
}
```

* `"RigidBodyTypes"` is the list of each rigid body type. Each rigid body type specifies a boundary of 2D points and is given a name within its json file. `path` is the disk location of the json file specifying the rigid body type. Boundary coordinates are specified in a local frame where the origin should be roughly in the middle of the boundary.

* `"ConnectivityGraph"` dictates which rigid bodies may interact with one another. The connectivity graph is a dictionary of vertices in the graph keyed by the human readable name of each vertex. Each vertex contains:
    * `"body_name"` The name of the rigid body type whose boundary is used for this vertex
    * `"edges"` A list of edges from the current vertex ending in vertices specified by a human readable name
    * `"configuration"` The configuration of this rigid body in world coordinates. `theta` is the angle in radians that the boundary of the rigid body has been rotated about its local frame origin.

* `"FixedRigidBodyNode"` The name of the vertex in the chain's graph which is assumed to be fixed. If no node should be fixed, you can make a "ghost" node and fix that. See the flock control example for more on this.

## PNG

You have the option of specifying a chain as a PNG image where all pixels which are black are used to create a chain with only two types of rigid bodies which are the same shape as a pixel (square).
