import cv2
import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
import os
import re
import glfw
import json
import math
import argparse
from PIL import Image

TEXTURE_FILE_NEG = "./mc_escher_bird_texture_neg.png"
TEXTURE_FILE_POS = "./mc_escher_bird_texture_pos.png"

def get_bound_box(list_of_point):
	x_min = float("inf")
	y_min = float("inf")
	x_max = float("-inf")
	y_max = float("-inf")
	for point in list_of_point:
		if point[0] < x_min:
			x_min = point[0]
		if point[0] > x_max:
			x_max = point[0]
		if point[1] < y_min:
			y_min = point[1]
		if point[1] > y_max:
			y_max = point[1]
	return (x_min, x_max, y_min, y_max)

def transfer_point_list(point_list, configuration, scale=10, screen_offset=None):
	if screen_offset == None:
		screen_offset = [0 for _ in range(len(point_list[0]))]
	result_list = []
	c = configuration
	cos_angle = math.cos(c[2])
	sin_angle = math.sin(c[2])
	rotation_matrix = np.array([[cos_angle,-sin_angle],[sin_angle,cos_angle]])
	for point in point_list:
		# scaling first, at origin
		after_scale = [c*scale for c in point]
		# rotation
		after_rotate = list(np.dot(rotation_matrix, np.array(after_scale)))
		# translate
		after_translate = [c[i]*scale+after_rotate[i]+screen_offset[i] for i in range(len(after_rotate))]
		# Add to result
		result_list.append(after_translate)
	return result_list

def add_polygon(texture_file_name, type_name, configurations, scale, screen_offset):
	image = Image.open(texture_file_name).convert("RGBA")
	texture_size = image.size
	img_matrix = np.array(list(image.getdata()), dtype=np.uint8)
# 	for i in range(img_matrix.shape[0]):
# 		if (img_matrix[i] == 0).all():
# 			img_matrix[i] = np.array([255, 255, 255, 0])
	# Inverse texture, because we will inverse output image.
	img_matrix = img_matrix.reshape([texture_size[0], texture_size[1], -1])[::-1, :, :]
	texture_img = img_matrix.tostring()


	textureID1 = glGenTextures(1)
	glBindTexture(GL_TEXTURE_2D, textureID1)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
	glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_size[0], texture_size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_img)

	for node in configurations:
		if configurations[node]["type"] == type_name:
			original_boundary = configurations[node]["old_boundary"]
			x_min, x_max, y_min, y_max = get_bound_box(original_boundary)
			x_len = x_max - x_min
			y_len = y_max - y_min
			texCoords = []
			for point in original_boundary:
				texCoords.append([(point[0]-x_min)/x_len, (point[1]-y_min)/y_len])
			new_point = transfer_point_list(configurations[node]["old_boundary"], configurations[node]["configuration"], scale, screen_offset)
			length = len(new_point)
			glBegin(GL_POLYGON)
			for i in range(length):
				glTexCoord2f(*texCoords[i]); glVertex3f(new_point[i][0], new_point[i][1], 0)
			glEnd()

def main(folder_dir, configuration_re, video_name):
	# This is the maximum resolution for glfw.
	DISPLAY_WIDTH = 1780
	DISPLAY_HEIGHT = 1780
	border_width = 10
	white_border_width = 30
	scaling = 22

	configuration_files = [filepath for filepath in os.listdir(folder_dir) if re.search(configuration_re, filepath) != None]
	configuration_files = sorted(configuration_files)
	total_length = len(configuration_files)
	print("Total frames: ", total_length)
	# Output to video
	VIDEO_WIDTH = DISPLAY_WIDTH + white_border_width * 2
	VIDEO_HEIGHT = DISPLAY_HEIGHT + white_border_width * 2
	video = cv2.VideoWriter(video_name, cv2.VideoWriter_fourcc(*"DIVX"), 30, (VIDEO_WIDTH, VIDEO_HEIGHT))

	for index, configuration_file in enumerate(configuration_files):
		print("Processing {0:03.2f}%".format(index/total_length*100))
		with open(configuration_file, "r") as f:
			configurations = json.load(f)

		if not glfw.init():
			return
		# Set window invisible
		glfw.window_hint(glfw.VISIBLE, False)
		# Create a invisible hidden window
		window = glfw.create_window(DISPLAY_WIDTH, DISPLAY_HEIGHT, "hidden window", None, None)
		if not window:
			glfw.terminate()
			return

		glfw.make_context_current(window)

		# Init gl
		glViewport(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT)
		glMatrixMode(GL_PROJECTION)
		glClearColor(255, 255, 255, 0)
		glClearDepth(1.0)
		glEnable(GL_DEPTH_TEST)
		glDepthFunc(GL_LEQUAL)
		glEnable(GL_COLOR_MATERIAL)
		glEnable(GL_LIGHT0)
		# Important, this will enable transparency in our image
		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		glLoadIdentity()
		gluOrtho2D(0, DISPLAY_WIDTH, 0, DISPLAY_HEIGHT)
		glEnable(GL_TEXTURE_2D)
		glClear(GL_COLOR_BUFFER_BIT)

		add_polygon(TEXTURE_FILE_POS, "mc_escher_bird_pos", configurations, scaling, [DISPLAY_WIDTH/4, DISPLAY_HEIGHT/2])
		add_polygon(TEXTURE_FILE_NEG, "mc_escher_bird_neg", configurations, scaling, [DISPLAY_WIDTH/4, DISPLAY_HEIGHT/2])

		glDisable(GL_BLEND)
		# Draw border
		glBegin(GL_QUADS)
		glColor3f(0, 0, 0)
		glVertex3f(0, 0, 0)
		glVertex3f(DISPLAY_WIDTH, 0, 0)
		glVertex3f(DISPLAY_WIDTH, border_width, 0)
		glVertex3f(0, border_width, 0)
		glEnd()
		glBegin(GL_QUADS)
		glColor3f(0, 0, 0)
		glVertex3f(0, 0, 0)
		glVertex3f(border_width, 0, 0)
		glVertex3f(border_width, DISPLAY_HEIGHT, 0)
		glVertex3f(0, DISPLAY_HEIGHT, 0)
		glEnd()
		glBegin(GL_QUADS)
		glColor3f(0, 0, 0)
		glVertex3f(0, DISPLAY_HEIGHT, 0)
		glVertex3f(DISPLAY_WIDTH, DISPLAY_HEIGHT, 0)
		glVertex3f(DISPLAY_WIDTH, DISPLAY_HEIGHT-border_width, 0)
		glVertex3f(0, DISPLAY_HEIGHT-border_width, 0)
		glEnd()
		glBegin(GL_QUADS)
		glColor3f(0, 0, 0)
		glVertex3f(DISPLAY_WIDTH, DISPLAY_HEIGHT, 0)
		glVertex3f(DISPLAY_WIDTH-border_width, DISPLAY_HEIGHT, 0)
		glVertex3f(DISPLAY_WIDTH-border_width, 0, 0)
		glVertex3f(DISPLAY_WIDTH, 0, 0)
		glEnd()

		image_buffer = glReadPixels(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, OpenGL.GL.GL_RGB, OpenGL.GL.GL_UNSIGNED_BYTE)
		image = (np.frombuffer(image_buffer, dtype=np.uint8).reshape(DISPLAY_WIDTH, DISPLAY_HEIGHT, 3))[::-1, :, ::-1]

		canvas_shape = list(image.shape)
		canvas_shape[0] += white_border_width*2
		canvas_shape[1] += white_border_width*2
		canvas = np.ones(canvas_shape, dtype=np.uint8)*255
		canvas[white_border_width:canvas_shape[0]-white_border_width, white_border_width:canvas_shape[1]-white_border_width]=image

		video.write(canvas)

		glfw.destroy_window(window)
		glfw.terminate()

	cv2.destroyAllWindows()
	video.release()


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Use configuration of scene to create rendered images")
	parser.add_argument("folder_path", metavar="folder_path", type=str, help="path to configuration files")
	parser.add_argument("configuration_re", metavar="configuration_re", type=str, help="configuration file name regular expression")
	parser.add_argument("video_name", metavar="video_name", type=str, help="output video path")

	arg = parser.parse_args()

	main(arg.folder_path, arg.configuration_re, arg.video_name)
