import json
import sys
import math
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import collections as mc

def plot(jsonFiles):
	plotsNum = len(jsonFiles)
	plotSideNum = max(math.floor(math.sqrt(plotsNum)), 1)
	fig, ax = plt.subplots(nrows=plotSideNum, ncols=math.ceil(plotsNum/plotSideNum))
	onlyOne = True
	modelBoundary = []
	print("One by one model")
	if (isinstance(ax, np.ndarray)):
		onlyOne = False
		ax = ax.flatten()
	for count, jsonFile in enumerate(jsonFiles):
		with open(jsonFile, "r") as f:
			data = json.load(f)
		boundaryPoints = [[a["x"], a["y"]] for a in data["boundary"]]
		boundary = [[boundaryPoints[i], boundaryPoints[i+1]] for i in range(len(boundaryPoints)-1)]
		modelBoundary.append(boundary)
		boundary_c = np.array([(1, 0, 0, 1) for _ in range(len(boundary))])
		lc = mc.LineCollection(boundary, colors=boundary_c, linewidths=2)
		if onlyOne:
			ax.add_collection(lc)
			ax.autoscale()
			ax.margins(0.1)
			ax.axis('square')
		else:
			ax[count].add_collection(lc)
			ax[count].autoscale()
			ax[count].margins(0.1)
			ax[count].axis('square')
	plt.show()
	if not onlyOne:
		# If not only one model given, put them together
		fig2, ax2 = plt.subplots()
		def shiftObj(objIndex, offsetX, offsetY):
			if len(modelBoundary) > objIndex:
				modelBoundary[objIndex] = [[[v[0]+offsetX, v[1]+offsetY] for v in e] for e in modelBoundary[objIndex]]
		# For mc escher locating purpose
		# shiftObj(1, 0, -16.80)
		# shiftObj(0, -0.2, 0.1)
		# shiftObj(2, 6.00, -8.60)
		# shiftObj(3, 5.90, 8.30)
		# shiftObj(4, 18.8, -14.6)
		# shiftObj(5, 18.6, 2.30)
		# shiftObj(6, 24.80, -6.4)
		# shiftObj(7, 24.70, 10.50)
		# shiftObj(8, 37.60, -12.4)
		# shiftObj(9, 37.40, 4.5)
		# shiftObj(10, 43.60, -4.2)
		# shiftObj(11, 43.50, 12.70)
		for count, b in enumerate(modelBoundary):
			uniColor = (random.random()/2+0.5, random.random()/2+0.5, random.random()/2+0.5, 1)
			boundary_c = np.array([uniColor for _ in range(len(b))])
			lc = mc.LineCollection(b, colors = boundary_c, linewidths = 2)
			ax2.add_collection(lc)
		ax2.autoscale()
		ax2.margins(0.1)
		ax2.axis('square')
		plt.show()

if __name__ == "__main__":
	if len(sys.argv) <= 1:
		print("json file missing")
	else:
		plot(sys.argv[1:])